<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeBanner extends Model
{
    protected $fillable = ['image', 'alt_en', 'description_en', 'alt_ar', 'description_ar'];

    public $timestamps = false;

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/home-banners/'.$value),
                'thumb'=>asset('storage/home-banners/thumb/'.$value)
            ];
        }
        return $value;
    }

}

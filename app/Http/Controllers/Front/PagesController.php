<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PagesController extends Controller
{
    public function show($slug){
        $page = Page::whereSlug($slug)->first();
        if(!$page){
            abort(404);
        }
        $view = 'show';
        if($page->slug){
            $view = $page->slug;
        }
        return view('front.pages.'.$view)->with('page', $page);
    }
}

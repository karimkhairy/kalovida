<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Certificate;

class CertificatesController extends Controller
{
    public function index(){
        $certificates = Certificate::all();
        return view('front.certificates.index')->with('certificates', $certificates);
    }


}

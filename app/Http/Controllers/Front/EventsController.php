<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;

class EventsController extends Controller
{
    public function index(){
        $events = Event::where('section', 'hajj')->paginate(10);
        return view('front.events.index')->with('events', $events);
    }

    public function show($slug){
        $event = Event::whereSlug($slug)->first();
        if(!$event){
            abort(404);
        }
        return view('front.events.show')->with('event', $event);
    }
}

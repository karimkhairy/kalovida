<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;
use App\Category;

class ProgramsController extends Controller
{
    public function categories(){
        $categories = Category::all();
        return view('front.programs.categories')->with('categories', $categories);
    }

    public function index($category_slug){
        $category = Category::where('slug', $category_slug)->first();
        if(!$category){
            abort(404);
        }
        $programs = Program::where('category_id', $category->id)->paginate(15);
        return view('front.programs.index')->with('programs', $programs)->with('category', $category);
    }

    public function show($slug){
        $program = Program::whereSlug($slug)->first();
        if(!$program){
            abort(404);
        }
        return view('front.programs.show')->with('program', $program);
    }
}

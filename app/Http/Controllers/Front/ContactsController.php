<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Mail\ContactUs;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    function show(){
        $contact = Contact::first();
        return view('front.contacts.show')->with('contact', $contact);
    }

    function send(Request $request){
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required|email',
            'comment'=>'required|min:50',
            'g-recaptcha-response'=>'required|recaptcha'
        ]);
        $message = (object) $request->only(['name', 'email', 'comment']);
       Mail::send(new ContactUs($message));
    flash()->success(__('message sent'));

        return redirect()->back();
    }
}

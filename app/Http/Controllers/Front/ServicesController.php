<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;

class ServicesController extends Controller
{
    public function index(){
        $services = Service::all();
        return view('front.services.index')->with('services', $services);
    }

    public function show($slug){
        $service = Service::whereSlug($slug)->first();
        if(!$service){
            abort(404);
        }
        return view('front.services.show')->with('service', $service);
    }
}

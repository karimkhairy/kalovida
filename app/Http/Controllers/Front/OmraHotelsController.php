<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hotel;

class OmraHotelsController extends Controller
{
    public function index(){
        $hotels = Hotel::where('section', 'omra')->paginate(10);
        return view('front.omra-hotels.index')->with('hotels', $hotels);
    }

    public function show($slug){
        $hotel = Hotel::whereSlug($slug)->first();
        if(!$hotel){
            abort(404);
        }
        return view('front.omra-hotels.show')->with('hotel', $hotel);
    }
}

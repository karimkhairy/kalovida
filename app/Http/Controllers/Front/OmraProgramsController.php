<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;

class OmraProgramsController extends Controller
{
    public function index(){
        $programs = Program::where('section', 'omra')->paginate(10);
        return view('front.omra-programs.index')->with('programs', $programs);
    }

    public function show($slug){
        $program = Program::whereSlug($slug)->first();
        if(!$program){
            abort(404);
        }
        return view('front.omra-programs.show')->with('program', $program);
    }
}

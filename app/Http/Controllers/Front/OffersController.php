<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;

class OffersController extends Controller
{
    public function index(){
        $offers = Offer::paginate(9);
        return view('front.offers.index')->with('offers', $offers);
    }

    public function show($slug){
        $offer = Offer::whereSlug($slug)->first();
        if(!$offer){
            abort(404);
        }
        return view('front.offers.show')->with('offer', $offer);
    }
}

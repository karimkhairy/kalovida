<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;
use App\Booking;

class BookingsController extends Controller
{
    public function create($program_slug){
        $program = Program::whereSlug($program_slug)->first();;
        if(!$program){
            abort(404);
        }
        return view('front.bookings.create')->with('program', $program);
    }

    public function store(Request $request, $program_slug){
        $program = Program::whereSlug($program_slug)->first();;
        if(!$program){
            abort(404);
        }

        $booking = new Booking;
        $booking->program_id = $program->id;
        $booking->user_id = $request->user()->id;
        $booking->save();

        flash()->success(__('Your booking sent successfully'));
        return redirect(i18nUrl('programs/'.$program->slug.'/book/response'));
    }

    public function response($program_slug){
        $program = Program::whereSlug($program_slug)->first();;
        if(!$program){
            abort(404);
        }
        return view('front.bookings.response')->with('program', $program);
    }



}

<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;

class HajjProgramsController extends Controller
{
    public function index(){
        $programs = Program::where('section', 'hajj')->paginate(10);
        return view('front.hajj-programs.index')->with('programs', $programs);
    }

    public function show($slug){
        $program = Program::whereSlug($slug)->first();
        if(!$program){
            abort(404);
        }
        return view('front.hajj-programs.show')->with('program', $program);
    }
}

<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hotel;

class HajjHotelsController extends Controller
{
    public function index(){
        $hotels = Hotel::where('section', 'hajj')->paginate(10);
        return view('front.hajj-hotels.index')->with('hotels', $hotels);
    }

    public function show($slug){
        $hotel = Hotel::whereSlug($slug)->first();
        if(!$hotel){
            abort(404);
        }
        return view('front.hajj-hotels.show')->with('hotel', $hotel);
    }
}

<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Newsletter;

class NewslettersController extends Controller
{
    public function index(){
        $newsletters = Newsletter::paginate(9);
        return view('front.newsletters.index')->with('newsletters', $newsletters);
    }

    public function show($slug){
        $newsletter = Newsletter::whereSlug($slug)->first();
        if(!$newsletter){
            abort(404);
        }
        return view('front.newsletters.show')->with('newsletter', $newsletter);
    }
}

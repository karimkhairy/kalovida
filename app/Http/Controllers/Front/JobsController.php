<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Mail\JobRequest;
use Illuminate\Support\Facades\Mail;

class JobsController extends Controller
{
    function show(){
        return view('front.jobs.show');
    }

    function send(Request $request){
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required|email',
            'address'=>'required',
            'phone'=>'required|numeric',
            'details'=>'required|min:50',
            'g-recaptcha-response'=>'required|recaptcha'
        ]);
        $message = (object) $request->only(['name', 'email', 'phone', 'address', 'details']);
       Mail::send(new JobRequest($message));
    flash()->success(__('message sent'));

        return redirect()->back();
    }
}

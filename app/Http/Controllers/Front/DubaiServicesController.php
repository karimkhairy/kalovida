<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DubaiService;

class DubaiServicesController extends Controller
{
    public function index(){
        $services = DubaiService::all();
        return view('front.dubai-services.index')->with('services', $services);
    }

    public function show($slug){
        $service = DubaiService::whereSlug($slug)->first();
        if(!$service){
            abort(404);
        }
        return view('front.dubai-services.show')->with('service', $service);
    }
}

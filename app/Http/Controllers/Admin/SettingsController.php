<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Http\Traits\ImageTrait;

class SettingsController extends Controller
{
    use ImageTrait;

    public function index(){
        return view('admin.settings.index');
    }


    public function editSocialMedia(){
        $setting = Setting::first();
        if(!$setting){
            $setting = new Setting;
        }
        return view('admin.settings.social-media')->with('setting', $setting->social);
    }

    public function updateSocialMedia(Request $request){
        $this->validate($request, [
            'facebook'=>'nullable|url',
            'youtube'=>'nullable|url',
            'twitter'=>'nullable|url',
            'instagram'=>'nullable|url',
        ]);
        $setting = Setting::first();
        if(!$setting){
            $setting = new Setting;
        }

        $setting->social = $request->except('_token');

        $setting->save();
        return redirect()->back();
    }


    public function editPagesHeader(){
        $setting = Setting::first();
        if(!$setting){
            $setting = new Setting;
        }
        return view('admin.settings.pages-header')->with('setting', $setting->pages_header);
    }

    public function updatePagesHeader(Request $request){
        $this->validate($request, [
            'page'=>'required',
            'image'=>'required|image|max:2000',
        ]);

        $setting = Setting::first();
        if(!$setting){
            $setting = new Setting;
        }

        $pages_header = $setting->pages_header;

        if(!empty($pages_header[$request->page])){
            $this->deleteImage('pages-header', $pages_header[$request->page]);
        }

        
        if ($request->file('image')->isValid()) {
            $pages_header[$request->page] = $this->uploadWithoutResize($request->image, 'pages-header');
        }
        $setting->pages_header = $pages_header;
        $setting->save();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $query = User::query();

        if($request->filled('key')){
            $query->where('name', 'like', "%{$request->key}%")
                ->orWhere('email', 'like', "%{$request->key}%" );
        }

        $users = $query->paginate(config('system.limit_per_page'));
        return view('admin.users.index')->with('users', $users);
    }

    public function create(){
        return view('admin.users.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required',
            'password'=>'required|min:6|max:20|confirmed'
        ]);

        $user = User::create($request->all());
        flash()->success('data saved successfully');
        return redirect()->route('admin.users.index');
    }

    public function edit($id){
        $user = User::findOrFail($id);
        return view('admin.users.edit')->with('user', $user);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required',
            'password'=>'nullable|min:6|max:20|confirmed'
        ]);

        $user = User::findOrFail($id);

        $user->update($request->all());
        flash()->success('data saved successfully');
        return redirect()->route('admin.users.index');
    }


    public function destroy($id){
        $user = User::findOrFail($id);
        $user->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

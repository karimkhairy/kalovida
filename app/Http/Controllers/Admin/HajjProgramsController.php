<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;
use App\Http\Traits\ImageTrait;

class HajjProgramsController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Program::where('section', 'hajj');

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $programs = $query->paginate(config('system.limit_per_page'));
        return view('admin.hajj-programs.index')->with('programs', $programs);
    }

    public function create(){
        return view('admin.hajj-programs.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
        ]);

        $inputs = $request->except(['image']);
        $inputs['section'] = 'hajj';
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'programs', 370, 350);
            }
        }

        $program = Program::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.hajj-programs.index');
    }

    public function edit($id){
        $program = Program::findOrFail($id);
        return view('admin.hajj-programs.edit')->with('program', $program);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $program = Program::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('programs', $program->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'programs', 370, 350);
        }

        $program->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.hajj-programs.index');
    }

    public function destroy($id){
        $program = Program::findOrFail($id);
        $this->deleteImage('programs', $program->getOriginal('image'));
        $program->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Certificate;
use App\Http\Traits\ImageTrait;

class CertificatesController extends Controller
{
    use ImageTrait;

    public function index(){
        $certificates = Certificate::all();
        return view('admin.certificates.index')->with('certificates', $certificates);
    }

    public function create(){
        return view('admin.certificates.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required'
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'certificates', 350, 240);
            }
        }

        $certificate = Certificate::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.certificates.index');
    }

    public function edit($id){
        $certificate = Certificate::findOrFail($id);
        return view('admin.certificates.edit')->with('certificate', $certificate);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required'
        ]);

        $inputs = $request->except(['image']);

        $certificate = Certificate::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('certificates', $certificate->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'certificates', 350, 240);
        }

        $certificate->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.certificates.index');
    }

    public function destroy($id){
        $certificate = Certificate::findOrFail($id);
        $this->deleteImage('certificates', $certificate->getOriginal('image'));
        $certificate->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

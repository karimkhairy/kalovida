<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Http\Traits\ImageTrait;

class CategoriesController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Category::query();

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $categories = $query->paginate(config('system.limit_per_page'));
        return view('admin.categories.index')->with('categories', $categories);
    }

    public function create(){
        return view('admin.categories.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required'
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'categories', 370);
            }
        }

        $category = Category::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.categories.index');
    }

    public function edit($id){
        $category = Category::findOrFail($id);
        return view('admin.categories.edit')->with('category', $category);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required'
        ]);

        $inputs = $request->except(['image']);

        $category = Category::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('categories', $category->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'categories', 370, 201);
        }

        $category->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.categories.index');
    }

    public function destroy($id){
        $category = Category::findOrFail($id);
        $this->deleteImage('categories', $category->getOriginal('image'));
        $category->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Http\Traits\ImageTrait;

class PhotosController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Photo::query();
        if($request->filled('k') && $request->filled('v')){
            $query->where($request->k, $request->v);
        }else{
            $query->whereNull('hotel_id');
        }
        return $query->get();
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'k'=>'required',
            'v'=>'required'
        ]);


        $data = [
            'alt_en'=>$request->alt_en,
            'alt_ar'=>$request->alt_ar,
            $request->k => $request->v
        ];

        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $data['image'] = $this->uploadImageAndFit($request->image, 'photos', 200);
            }
        }


        $photo = Photo::create($data);
        return $photo;
    }


    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $photo = Photo::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('photos', $photo->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'photos', 200);
        }

        $photo->update($inputs);
        return $photo;
    }

    public function destroy($id){
        $photo = Photo::findOrFail($id);
        $this->deleteImage('photos', $photo->getOriginal('image'));
        $photo->delete();
        return 'success';
    }
}

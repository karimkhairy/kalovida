<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;

class ContactsController extends Controller
{
    public function edit($id){
        $contact = Contact::findOrFail($id);
        return view('admin.contacts.edit')->with('contact', $contact);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'address_en'=>'required',
            'address_ar'=>'required',
            'phone'=>'required',
            'email'=>'required|email'
        ]);

        $inputs = $request->all();

        $contact = Contact::findOrFail($id);


        $contact->update($inputs);
        flash()->success('data saved successfully');
        return redirect()->back();
    }
}

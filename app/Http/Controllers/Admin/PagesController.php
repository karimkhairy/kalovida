<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\Http\Traits\ImageTrait;

class PagesController extends Controller
{
    use ImageTrait;

    public function edit($id){
        $page = Page::findOrFail($id);
        return view('admin.pages.edit')->with('page', $page);
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'title_en'=>'required',
            'title_ar'=>'required',
            'description_en'=>'required',
            'description_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $page = Page::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('pages', $page->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'pages', 100);
        }

        $page->update($inputs);
        flash()->success('data saved successfully');
        return redirect()->back();
    }
}

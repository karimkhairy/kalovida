<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;
use App\Http\Traits\ImageTrait;

class OffersController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Offer::query();

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $offers = $query->paginate(config('system.limit_per_page'));
        return view('admin.offers.index')->with('offers', $offers);
    }

    public function create(){
        return view('admin.offers.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'offers', 370, 265);
            }
        }

        $offer = Offer::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.offers.index');
    }

    public function edit($id){
        $offer = Offer::findOrFail($id);
        return view('admin.offers.edit')->with('offer', $offer);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $offer = Offer::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('offers', $offer->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'offers', 370, 265);
        }

        $offer->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.offers.index');
    }

    public function destroy($id){
        $offer = Offer::findOrFail($id);
        $this->deleteImage('offers', $offer->getOriginal('image'));
        $offer->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }

    public function showInHome($id){
        $offer = Offer::findOrFail($id);
        $offer->show_in_home = !$offer->show_in_home;
        $offer->save();
        flash()->success('data saved successfully');
        return redirect()->back();
    }
}

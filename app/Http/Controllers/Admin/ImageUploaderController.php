<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ImageTrait;

class ImageUploaderController extends Controller
{
    use ImageTrait;

    public function store(Request $request){
        $fileName = $this->uploadWithoutResize($request->image, 'ckeditor');
        return asset('storage/ckeditor/'.$fileName);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\Http\Traits\ImageTrait;

class OmraHotelsController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Hotel::where('section', 'omra');

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $hotels = $query->paginate(config('system.limit_per_page'));
        return view('admin.omra-hotels.index')->with('hotels', $hotels);
    }

    public function create(){
        return view('admin.omra-hotels.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
        ]);

        $inputs = $request->except(['image']);
        $inputs['section'] = 'omra';
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'hotels', 100);
            }
        }

        $hotel = Hotel::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.omra-hotels.index');
    }

    public function edit($id){
        $hotel = Hotel::findOrFail($id);
        return view('admin.omra-hotels.edit')->with('hotel', $hotel);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $hotel = Hotel::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('hotels', $hotel->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'hotels', 100);
        }

        $hotel->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.omra-hotels.index');
    }

    public function destroy($id){
        $hotel = Hotel::findOrFail($id);
        $this->deleteImage('hotels', $hotel->getOriginal('image'));
        $hotel->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

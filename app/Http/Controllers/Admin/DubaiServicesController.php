<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DubaiService;
use App\Http\Traits\ImageTrait;

class DubaiServicesController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = DubaiService::query();

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $services = $query->paginate(config('system.limit_per_page'));
        return view('admin.dubai-services.index')->with('services', $services);
    }

    public function create(){
        return view('admin.dubai-services.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'dubai-services', 370, 201);
            }
        }

        $service = DubaiService::create($inputs);
        flash()->success('data saved successfully');
        return redirect()->route('admin.dubai-services.index');
    }

    public function edit($id){
        $service = DubaiService::findOrFail($id);
        return view('admin.dubai-services.edit')->with('service', $service);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $service = DubaiService::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('services', $service->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'dubai-services', 370, 201);
        }

        $service->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.dubai-services.index');
    }

    public function destroy($id){
        $service = DubaiService::findOrFail($id);
        $this->deleteImage('dubai-services', $service->getOriginal('image'));
        $service->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

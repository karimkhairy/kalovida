<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Booking;

class BookingsController extends Controller
{

    public function index(Request $request){
        $query = Booking::query();

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }
        $query->orderBy('id', 'DESC');

        $bookings = $query->paginate(config('system.limit_per_page'));
        return view('admin.bookings.index')->with('bookings', $bookings);
    }

    public function show($id){
        $booking = Booking::findOrFail($id);

        return view('admin.bookings.show')->with('booking', $booking);
    }

    
}

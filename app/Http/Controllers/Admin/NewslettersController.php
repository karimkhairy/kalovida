<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Newsletter;
use App\Http\Traits\ImageTrait;

class NewslettersController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Newsletter::query();

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $newsletters = $query->paginate(config('system.limit_per_page'));
        return view('admin.newsletters.index')->with('newsletters', $newsletters);
    }

    public function create(){
        return view('admin.newsletters.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'newsletters', 100);
            }
        }

        $newsletter = Newsletter::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.newsletters.index');
    }

    public function edit($id){
        $newsletter = Newsletter::findOrFail($id);
        return view('admin.newsletters.edit')->with('newsletter', $newsletter);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $newsletter = Newsletter::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('newsletters', $newsletter->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'newsletters', 100);
        }

        $newsletter->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.newsletters.index');
    }

    public function destroy($id){
        $newsletter = Newsletter::findOrFail($id);
        $this->deleteImage('newsletters', $newsletter->getOriginal('image'));
        $newsletter->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\Http\Traits\ImageTrait;

class ServicesController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Service::query();

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $services = $query->paginate(config('system.limit_per_page'));
        return view('admin.services.index')->with('services', $services);
    }

    public function create(){
        return view('admin.services.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'services', 370);
            }
        }

        $service = Service::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.services.index');
    }

    public function edit($id){
        $service = Service::findOrFail($id);
        return view('admin.services.edit')->with('service', $service);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $service = Service::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('services', $service->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'services', 370, 201);
        }

        $service->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.services.index');
    }

    public function destroy($id){
        $service = Service::findOrFail($id);
        $this->deleteImage('services', $service->getOriginal('image'));
        $service->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

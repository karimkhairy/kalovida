<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Http\Traits\ImageTrait;

class EventsController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        $query = Event::query();

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $events = $query->paginate(config('system.limit_per_page'));
        return view('admin.events.index')->with('events', $events);
    }

    public function create(){
        return view('admin.events.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'events', 100);
            }
        }

        $event = Event::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.events.index');
    }

    public function edit($id){
        $event = Event::findOrFail($id);
        return view('admin.events.edit')->with('event', $event);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $event = Event::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('events', $event->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'events', 100);
        }

        $event->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.events.index');
    }

    public function destroy($id){
        $event = Event::findOrFail($id);
        $this->deleteImage('events', $event->getOriginal('image'));
        $event->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }

    public function showInHome($id){
        $event = Event::findOrFail($id);
        $event->show_in_home = !$event->show_in_home;
        $event->save();
        flash()->success('data saved successfully');
        return redirect()->back();
    }
}

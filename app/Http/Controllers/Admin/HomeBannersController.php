<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomeBanner;
use App\Http\Traits\ImageTrait;

class HomeBannersController extends Controller
{
    use ImageTrait;

    public function index(){
        $banners = HomeBanner::all();
        return view('admin.home-banners.index')->with('banners', $banners);
    }

    public function create(){
        return view('admin.home-banners.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000'
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'home-banners', 100);
            }
        }

        $banner = HomeBanner::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.home-banners.index');
    }

    public function edit($id){
        $banner = HomeBanner::findOrFail($id);
        return view('admin.home-banners.edit')->with('banner', $banner);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000'
        ]);

        $inputs = $request->except(['image']);

        $banner = HomeBanner::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('home-banners', $banner->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'home-banners', 100);
        }

        $banner->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.home-banners.index');
    }

    public function destroy($id){
        $banner = HomeBanner::findOrFail($id);
        $this->deleteImage('home-banners', $banner->getOriginal('image'));
        $banner->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

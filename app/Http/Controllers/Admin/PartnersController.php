<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partner;
use App\Http\Traits\ImageTrait;

class PartnersController extends Controller
{
    use ImageTrait;

    public function index(){
        $partners = Partner::all();
        return view('admin.partners.index')->with('partners', $partners);
    }

    public function create(){
        return view('admin.partners.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required'
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadWithoutResize($request->image, 'partners');
            }
        }

        $partner = Partner::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.partners.index');
    }

    public function edit($id){
        $partner = Partner::findOrFail($id);
        return view('admin.partners.edit')->with('partner', $partner);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required'
        ]);

        $inputs = $request->except(['image']);

        $partner = Partner::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('partners', $partner->getOriginal('image'));
            $inputs['image'] = $this->uploadWithoutResize($request->image, 'partners');
        }

        $partner->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.partners.index');
    }

    public function destroy($id){
        $partner = Partner::findOrFail($id);
        $this->deleteImage('partners', $partner->getOriginal('image'));
        $partner->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }
}

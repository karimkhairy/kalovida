<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;
use App\Category;
use App\Http\Traits\ImageTrait;

class ProgramsController extends Controller
{
    use ImageTrait;

    public function index(Request $request){
        if(!$request->filled('category_id')){
            abort(404);
        }
        $query = Program::where('section', 'main')->where('category_id', $request->category_id);

        if($request->filled('key')){
            $query->where('title_en', 'like', "%{$request->key}%")
                ->orWhere('title_ar', 'like', "%{$request->key}%" );
        }

        $programs = $query->paginate(config('system.limit_per_page'));

        $category = Category::findOrFail($request->category_id);
        return view('admin.programs.index')->with('programs', $programs)->with('category', $category);
    }

    public function create(Request $request){
        if(!$request->filled('category_id')){
            abort(404);
        }
        $category = Category::findOrFail($request->category_id);
        return view('admin.programs.create')->with('category', $category);
    }

    public function store(Request $request){

        $this->validate($request, [
            'image'=>'required|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
            'price'=>'nullable|numeric|min:1'
        ]);

        $inputs = $request->except(['image']);
        //upload image
        if($request->hasFile('image')){
            if ($request->file('image')->isValid()) {
                $inputs['image'] = $this->uploadImageAndFit($request->image, 'programs', 370,350);
            }
        }

        $program = Program::create($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.programs.index', ['category_id'=>$request->category_id]);
    }

    public function edit($id){
        $program = Program::findOrFail($id);
        return view('admin.programs.edit')->with('program', $program);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image'=>'nullable|image|max:2000',
            'title_en'=>'required',
            'title_ar'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
            'price'=>'nullable|numeric|min:1'
        ]);

        $inputs = $request->except(['image']);

        $program = Program::findOrFail($id);

        //upload image
        if($request->hasFile('image')){
            $this->deleteImage('programs', $program->getOriginal('image'));
            $inputs['image'] = $this->uploadImageAndFit($request->image, 'programs', 370,350);
        }

        $program->update($inputs);
        flash()->success('data saved successfully');
        return \redirect()->route('admin.programs.index', ['category_id'=>$program->category_id]);
    }

    public function destroy($id){
        $program = Program::findOrFail($id);
        $this->deleteImage('programs', $program->getOriginal('image'));
        $program->delete();
        flash()->success('data deleted successfully');
        return redirect()->back();
    }

    public function showInHome($id){
        $program = Program::findOrFail($id);
        $program->show_in_home = !$program->show_in_home;
        $program->save();
        flash()->success('data saved successfully');
        return redirect()->back();
    }
}

<?php
namespace App\Http\Traits;

use Storage;
use Image;

trait ImageTrait {
    protected function uploadImageAndFit($file, $path, $width = 100, $height = null){
        if($file->isValid()){
            $imagePath = $file->store($path);
            $imageName = str_replace($path.'/', '', $imagePath);

            $thumb = Image::make(Storage::path($imagePath))->fit($width, $height);//->save(Storage::path($path.'/thumb/'.$imageName));
            Storage::put($path.'/thumb/'.$imageName, (string) $thumb->encode());
            return $imageName;
        }
       return null;
    }

    protected function uploadWithoutResize($file, $path){
        if($file->isValid()){
            $imagePath = $file->store($path);
            $imageName = str_replace($path.'/', '', $imagePath);
            return $imageName;
        }
       return null;
    }

    protected function deleteImage($path, $imageName){
        return Storage::delete([
            $path.'/'.$imageName,
            $path.'/thumb/'.$imageName
        ]);
    }
}

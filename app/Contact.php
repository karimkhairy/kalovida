<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $timestamps = false;
    protected $fillable = ['address_en', 'address_ar', 'phone', 'email', 'map_url'];


}

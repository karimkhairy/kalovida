<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['title_en', 'title_ar', 'slug', 'description_en', 'description_ar', 'body_en', 'body_ar', 'image', 'slug'];

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/pages/'.$value),
                'thumb'=>asset('storage/pages/thumb/'.$value)
            ];
        }
        return $value;
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_en'
            ]
        ];
    }
}

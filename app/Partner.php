<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = ['image', 'title_en', 'description_en', 'title_ar', 'description_ar'];

    public $timestamps = false;

    public function getImageAttribute($value){
        if($value){
            return asset('storage/partners/'.$value);
        }
        return $value;
    }

}

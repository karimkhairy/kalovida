<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = ['image', 'title_en', 'description_en', 'title_ar', 'description_ar'];

    public $timestamps = false;

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/certificates/'.$value),
                'thumb'=>asset('storage/certificates/thumb/'.$value)
            ];
        }
        return $value;
    }

}

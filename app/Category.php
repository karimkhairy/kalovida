<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    protected $fillable = ['image', 'title_en', 'title_ar', 'slug'];

    public $timestamps = false;

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/categories/'.$value),
                'thumb'=>asset('storage/categories/thumb/'.$value)
            ];
        }
        return $value;
    }

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_en'
            ]
        ];
    }

}

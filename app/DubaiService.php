<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class DubaiService extends Model
{
    use Sluggable;

    protected $fillable = ['image', 'title_en', 'description_en', 'title_ar', 'description_ar', 'body_en', 'body_ar'];

    public $timestamps = false;

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/dubai-services/'.$value),
                'thumb'=>asset('storage/dubai-services/thumb/'.$value)
            ];
        }
        return $value;
    }

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_en'
            ]
        ];
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Newsletter extends Model
{
    use Sluggable;

    protected $fillable = ['image', 'title_en', 'description_en', 'title_ar', 'description_ar', 'body_en', 'body_ar'];

    public $timestamps = false;

    protected $dates = [
        'created_at',
    ];

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/newsletters/'.$value),
                'thumb'=>asset('storage/newsletters/thumb/'.$value)
            ];
        }
        return $value;
    }

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_en'
            ]
        ];
    }

}

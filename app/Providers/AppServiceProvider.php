<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerSubViews();
        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
    }


    private function registerSubViews(){
        Blade::include('admin.includes.breadcrumb', 'breadcrumb');
        Blade::include('admin.boot-form.input', 'input');
        Blade::include('admin.boot-form.button', 'button');
        Blade::include('admin.boot-form.select', 'select');
        Blade::include('admin.boot-form.link', 'link');
        Blade::include('admin.boot-form.action-link', 'actionLink');
        Blade::include('admin.boot-form.delete-link', 'deleteLink');
        Blade::include('admin.boot-form.textarea', 'textarea');
        Blade::include('admin.boot-form.thumb', 'thumb');
        Blade::include('front.includes.meta', 'meta');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['social', 'pages_header'];

    public $timestamps = false;

    protected $casts = [
        'social' => 'array',
        'pages_header' => 'array',
    ];

    
}

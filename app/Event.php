<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Event extends Model
{
    use Sluggable;

    protected $fillable = ['image', 'title_en', 'description_en', 'title_ar', 'description_ar', 'body_en', 'body_ar', 'show_in_home'];

    public $timestamps = false;

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/events/'.$value),
                'thumb'=>asset('storage/events/thumb/'.$value)
            ];
        }
        return $value;
    }

     /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_en'
            ]
        ];
    }

    public function photos(){
        return $this->hasMany('App\Photo');
    }
}

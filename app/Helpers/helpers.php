<?php

if (! function_exists('translate')) {
    function translate($model, $key) {
       return $model->{$key.'_'.app()->getLocale()};
    }
}

if (! function_exists('i18nUrl')) {
    function i18nUrl($link) {
       return url(app()->getLocale().'/'.$link);
    }
}

if (! function_exists('langSwitcherLink')) {
    function langSwitcherLink() {
       $lang = app()->getLocale();
       if($lang === 'en'){
           $toLang = ['/ar', 'العربية'];
       }else{
           $toLang = ['/en', 'English'];
       }
       $currentUrl = url()->full();
       $pos = strpos($currentUrl, '/'.$lang);
       if(!$pos){
           $currentUrl .= $toLang[0];
       }else{
        $currentUrl = substr_replace($currentUrl, $toLang[0], $pos, strlen('/'.$lang));
       }
       return '<a href="'.$currentUrl.'">'.$toLang[1].'<span class="fa fa-chevron-right"></span></a>';
    }
}

if (! function_exists('getSetting')) {
    function getSetting($key) {
       return \App\Setting::select('id', $key)->first();  
    }
}


if (! function_exists('getPageHeaderImage')) {
    function getPageHeaderImage($page) {
       $setting = getSetting('pages_header');
        if(!empty($setting->pages_header[$page])){
            return asset('storage/pages-header/'.$setting->pages_header[$page]);
        }
        return asset('front/img/bg_3.jpg');
    }
}
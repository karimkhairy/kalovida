<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Program extends Model
{
    use Sluggable;

    protected $fillable = ['image', 'title_en', 'description_en', 'title_ar', 'description_ar', 'body_en', 'body_ar', 'time_en', 'time_ar', 'section', 'show_in_home', 'price', 'category_id'];

    public $timestamps = false;

    public function getImageAttribute($value)
    {
        if ($value) {
            return [
                'original' => asset('storage/programs/' . $value),
                'thumb' => asset('storage/programs/thumb/' . $value)
            ];
        }
        return $value;
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_en'
            ]
        ];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['image', 'alt_en', 'alt_ar', 'hotel_id', 'event_id'];

    public $timestamps = false;

    public function getImageAttribute($value){
        if($value){
            return [
                'original'=>asset('storage/photos/'.$value),
                'thumb'=>asset('storage/photos/thumb/'.$value)
            ];
        }
        return $value;
    }

}

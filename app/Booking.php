<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    protected $fillable = ['program_id', 'user_id'];


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function program(){
        return $this->belongsTo('App\Program');
    }

}

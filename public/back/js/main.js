$.noConflict();

jQuery(document).ready(function($) {

	"use strict";

	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
		new SelectFx(el);
	} );

	jQuery('.selectpicker').selectpicker;


	$('#menuToggle').on('click', function(event) {
		$('body').toggleClass('open');
	});

	$('.search-trigger').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});

	// $('.user-area> a').on('click', function(event) {
	// 	event.preventDefault();
	// 	event.stopPropagation();
	// 	$('.user-menu').parent().removeClass('open');
	// 	$('.user-menu').parent().toggleClass('open');
	// });
    $('[data-toggle="tooltip"]').tooltip();

    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

    // colorbox
    //add color box functionality
    $(".cbox").colorbox({rel: 'group2', transition: "scale", scalePhotos: true, maxWidth: "70%", maxHeight: "70%"});
    $(".ajax").colorbox({innerWidth: 640, innerHeight: 390});
    $(".youtube").colorbox({iframe: true, innerWidth: 640, innerHeight: 390});
    $(".iframe").colorbox({iframe: true, width: "50%", height: "80%"});
	$(".bigiframe").colorbox({iframe: true, width: "90%", height: "90%"});
	
	CKEDITOR.replace( '.ckeditor' );

});

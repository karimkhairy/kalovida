-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 03, 2019 at 03:29 AM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kalovida`
--

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(200) NOT NULL,
  `title_en` varchar(200) DEFAULT NULL,
  `title_ar` varchar(200) DEFAULT NULL,
  `description_en` text,
  `description_ar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `image`, `title_en`, `title_ar`, `description_en`, `description_ar`) VALUES
(1, 'fwE5HyOfFtmwKHURoE66VwxcNoMis4A2QWjQ5NM6.jpeg', 'test', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `address_en` varchar(255) NOT NULL,
  `address_ar` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `map_url` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `address_en`, `address_ar`, `phone`, `email`, `map_url`) VALUES
(1, 'test', 'test ar', '+202 33031375', 'info@kalovidatravel.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13630.551693979587!2d30.795497538358596!3d31.34137115286541!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f6fb89f3bc4a35%3A0xe586a73102624848!2sDimru%2C+Al+Haddadi+WA+Izabeha%2C+Sidi+Salem%2C+Kafr+El+Sheikh+Governorate!5e0!3m2!1sen!2seg!4v1553967370697!5m2!1sen!2seg');

-- --------------------------------------------------------

--
-- Table structure for table `dubai_services`
--

CREATE TABLE `dubai_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `title_ar` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `body_en` text NOT NULL,
  `body_ar` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dubai_services`
--

INSERT INTO `dubai_services` (`id`, `title_en`, `title_ar`, `slug`, `description_en`, `description_ar`, `body_en`, `body_ar`, `image`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'test', 'jhkj', 'lkjljljk', '<p>uyuy</p>', '<p>yuy</p>', 'njrkiutTf2JTtKuqRBJQj0pkQEwvWvIMqMgAqmJg.jpeg', '2019-03-24 01:04:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(200) NOT NULL,
  `alt_en` varchar(200) DEFAULT NULL,
  `alt_ar` varchar(200) DEFAULT NULL,
  `description_en` text,
  `description_ar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `image`, `alt_en`, `alt_ar`, `description_en`, `description_ar`) VALUES
(1, 'J9b8JCNTRhE26XsDjhOMMoDvwnkbiHtB4nsOwRyC.jpeg', 'test', 'test', 'test1\r\ntest2\r\ntest3', 'test1 ar\r\ntest2 ar\r\ntest3 ar'),
(2, 'dQAiYghv59WhWt07XuLJKXuPQXDoZS3TMBRttAAx.jpeg', 'aa', 'aa', 'aa\r\naa\r\naa', 'aar\r\naar\r\naar');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `title_ar` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `body_en` text NOT NULL,
  `body_ar` text NOT NULL,
  `section` enum('hajj','omra') NOT NULL,
  `image` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `title_en`, `title_ar`, `slug`, `description_en`, `description_ar`, `body_en`, `body_ar`, `section`, `image`, `created_at`, `updated_at`) VALUES
(1, 'test hotel', 'test', 'test-hotel', 'test', 'test', '<p>test</p>', '<p>test</p>', 'hajj', '60kRetPjYFBbWZrh72e6LOwtYHHOmHL0Zp4Qexu9.jpeg', '2019-03-28 01:14:55', NULL),
(2, 'test omra hotel', 'test', 'test-omra-hotel', 'test', 'test', '<p>testtt</p>', '<p>test</p>', 'omra', 'ulpe9PpTDrwobejtQQg5EsAmA0aKaDR0AU0VGx0k.jpeg', '2019-03-29 20:58:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `title_ar` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `body_en` text NOT NULL,
  `body_ar` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `title_en`, `title_ar`, `slug`, `description_en`, `description_ar`, `body_en`, `body_ar`, `image`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'test', 'test', 'test', '<p>test</p>', '<p>test</p>', '2MIgIOhDHGsuEl0rSJNlRP3uI5ZUceYbhDhNoDNk.jpeg', '2019-03-24 02:15:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `title_ar` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `body_en` text NOT NULL,
  `body_ar` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `show_in_home` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `title_en`, `title_ar`, `slug`, `description_en`, `description_ar`, `body_en`, `body_ar`, `image`, `show_in_home`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'test', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 1, '2019-03-23 23:52:16', NULL),
(2, 'test', 'test', 'test2', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(3, 'test', 'test', 'test3', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(4, 'test', 'test', 'test4', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(5, 'test', 'test', 'test5', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(6, 'test', 'test', 'test6', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(7, 'test', 'test', 'test7', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(8, 'test', 'test', 'test8', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(9, 'test', 'test', 'test11', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(10, 'test', 'test', 'test12', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(11, 'test', 'test', 'test22', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL),
(12, 'test', 'test', 'test33', 'test', 'test', '<p>test</p>', '<p>test</p>', 'U0NJxVCoZSHtBLdhbBrNUyyy9orXZqp3KIoCyvnv.png', 0, '2019-03-23 23:52:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description_en` varchar(255) DEFAULT NULL,
  `description_ar` varchar(255) DEFAULT NULL,
  `body_en` text NOT NULL,
  `body_ar` text NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title_en`, `title_ar`, `slug`, `description_en`, `description_ar`, `body_en`, `body_ar`, `image`) VALUES
(1, 'About us', 'من نحن', 'about-us', 'test', 'test', '<p>&nbsp;</p><p><strong>test</strong></p>', '<p>test</p>', '7IN1nH8X4D4rmIR3UB7V6cyEhTMOwoAZn3gs67gl.jpeg'),
(2, 'hajj-info', 'معلومات عن الحج', 'hajj-info', 'test', 'test', '<p>test</p>', '<p>test</p>', 'ywMwSRL3emwLHL99xdDsQr7gWf8x5lORx9jwSVlG.jpeg'),
(3, 'umra-info', 'معلومات عن العمرة', 'umra-info', 'test', 'test', '<p>test</p>', '<p>test</p>', 'S2UPiaWhg45NpmnGIra6l4f7wfYbMhyHw9efSO1r.jpeg'),
(4, 'videos', 'معلومات عن العمرة', 'videos', 'test', 'test', '<h2>Best Goals</h2><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=bKOTKHtbM54\"></oembed></figure><p>&nbsp;</p><h3>Best Goals 2</h3><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=HcCv2S_syIk\"></oembed></figure>', '<h2>افضل اهداف 1</h2><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=bKOTKHtbM54\"></oembed></figure><p>&nbsp;</p><h3>افضل اهداف2</h3><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=HcCv2S_syIk\"></oembed></figure>', 'S2UPiaWhg45NpmnGIra6l4f7wfYbMhyHw9efSO1r.jpeg'),
(5, 'Our mission', 'Our mission', 'our-mission', 'Our Mission is to perform and deliver excellent quality service to our clients. With this objective, we aim to earn the reputation as \"Your Preferred Travel Agency\".', 'Our Mission is to perform and deliver excellent quality service to our clients. With this objective, we aim to earn the reputation as \"Your Preferred Travel Agency\".', '<p>mession body</p>', '<p>mession body</p>', 'S2UPiaWhg45NpmnGIra6l4f7wfYbMhyHw9efSO1r.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(200) NOT NULL,
  `title_en` varchar(200) DEFAULT NULL,
  `title_ar` varchar(200) DEFAULT NULL,
  `description_en` text,
  `description_ar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `image`, `title_en`, `title_ar`, `description_en`, `description_ar`) VALUES
(1, 'yC4khh0fVbmfuveqxvJhzYRwAswyIy7YLWQ6SQca.jpeg', 'test', 'test', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(200) NOT NULL,
  `alt_en` varchar(200) DEFAULT NULL,
  `alt_ar` varchar(200) DEFAULT NULL,
  `hotel_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `image`, `alt_en`, `alt_ar`, `hotel_id`) VALUES
(1, 'FbSOrqxnkbbq7Gfw9A0SKE9hWkbn8JWnBK3va95y.jpeg', NULL, NULL, 1),
(2, 'Nh9pFWaPgPmBC1pC8H2ALxikmVkDJW6haN7tdT7l.jpeg', 'test', 'test', NULL),
(5, 'WWcm8ehLbSl3kxHDrux8qv12SEyyGE4lrXUEwHxu.jpeg', NULL, NULL, 2),
(6, 'RG1tVNR8XdDhqVw3JblqDOzf3Q6M4dXQLhX2rtSn.jpeg', NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `title_ar` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `time_en` varchar(100) DEFAULT NULL,
  `time_ar` varchar(100) DEFAULT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `body_en` text NOT NULL,
  `body_ar` text NOT NULL,
  `price` decimal(5,2) NOT NULL DEFAULT '0.00',
  `section` enum('main','hajj','omra') NOT NULL DEFAULT 'main',
  `image` varchar(200) NOT NULL,
  `show_in_home` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `title_en`, `title_ar`, `slug`, `time_en`, `time_ar`, `description_en`, `description_ar`, `body_en`, `body_ar`, `price`, `section`, `image`, `show_in_home`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'test', 'test', 'test', 'test', 'test', '<p>test</p>', '<p>test</p>', '0.00', 'main', 'hSnY3UhGGIGrFPELXZDYWSEUvn7KydLElE5cMokq.jpeg', 1, '2019-03-24 00:12:26', NULL),
(2, 'test', 'test', 'test-1', 'test', 'test', 'test', 'test', '<p>test</p>', '<p>test</p>', '0.00', 'main', 'wANaXvAi1BDO6Q2i8lb6Wm5RXxefzh0Bjm1NjRyK.jpeg', 1, '2019-03-24 00:12:54', NULL),
(3, 'test', 'test', 'test-2', 'test', 'test', 'test', 'test', '<p>test</p>', '<p>test</p>', '0.00', 'main', 'nRCvSPC8BfS5gLRw3zjy9U4cFxQy1dijd70gkSn7.jpeg', 1, '2019-03-28 00:31:32', NULL),
(4, 'test', 'test', 'test-3', 'test', 'test', 'test', 'test', '<p>test</p>', '<p>test</p>', '0.00', 'hajj', 'bMpXUa0GJErOjYAgaO6tI7bW7XsAIjnJ7233OGMs.png', 1, '2019-03-28 00:32:26', NULL),
(5, 'test', 'test', 'test-4', 'test', 'test', 'test', 'test', '<p>test</p>', '<p>test</p>', '0.00', 'omra', 'ouFUYTtOy1CFjmxZ1UiTyuG2hauzXGHREB5Ud1dZ.jpeg', 0, '2019-03-28 00:36:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `title_ar` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `body_en` text NOT NULL,
  `body_ar` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title_en`, `title_ar`, `slug`, `description_en`, `description_ar`, `body_en`, `body_ar`, `image`, `created_at`, `updated_at`) VALUES
(1, 'testen', 'test ar', 'testen', 'desen', 'descar', '<figure class=\"image\"><img src=\"http://localhost:8000/storage/ckeditor/KuFOeeuF6CpsUsb8dsDqlaOVCmvxnBONn1MXYnx3.jpeg\" alt=\"55\"></figure><p>bodyen</p>', '<p>&nbsp;</p><p>bodyar</p><p><br>&nbsp;</p>', 'IyfV1CMenPb3ED2ULJVmjpvghTYHQKdC7w68rOxP.png', '2019-03-23 21:23:02', NULL),
(2, 'test', 'test', 'test', 'est', 'set', '<p>wet</p>', '<p>wet</p>', 'oGDF98lAZZX3z3hsPRmlSVDLKPm7va7gQPLlKFt9.png', '2019-03-24 01:02:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed Galal', 'a.galal201@gmail.com', 'admin', '2019-03-17 22:00:00', '$2y$10$EcI1.9/HnIrpWi3w2UsR8e42WbgKWbHmjGdr8C2brk/LetQfEZDBi', '5d9z1kazuLRGDt4DMyRIpOZcdHZkQ7NVkzBhl6kGqcYItakAlRDsU2Imkw2p', '2019-03-11 23:21:41', '2019-03-19 01:20:58'),
(2, 'Ali Ahmed', 'ali.ahmed@gmail.com', 'admin', '2019-03-17 22:00:00', '$2y$10$EcI1.9/HnIrpWi3w2UsR8e42WbgKWbHmjGdr8C2brk/LetQfEZDBi', 'xMroy7qRiyGedjDgZrJ9W1I4JfoIFkcrHtGuS71IVEK7kf3JljSsmrL0eUQl', '2019-03-11 23:21:41', '2019-03-11 23:21:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dubai_services`
--
ALTER TABLE `dubai_services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dubai_services`
--
ALTER TABLE `dubai_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('auth:web')->group(function () {
    Route::view('/', 'admin.dashboard.index')->name('dashboard.index');
    Route::resource('users', 'UsersController');
    Route::resource('home-banners', 'HomeBannersController');
    Route::resource('pages', 'PagesController');
    Route::resource('services', 'ServicesController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('image-uploader', 'ImageUploaderController')->only(['store', 'destroy']);
    Route::resource('offers', 'OffersController');
    Route::resource('events', 'EventsController');
    Route::get('events/{id}/show-in-home', 'EventsController@showInHome')->name('events.show-in-home');
    Route::get('offers/{id}/show-in-home', 'OffersController@showInHome')->name('offers.show-in-home');
    Route::resource('programs', 'ProgramsController');
    Route::get('programs/{id}/show-in-home', 'ProgramsController@showInHome')->name('programs.show-in-home');
    Route::resource('certificates', 'CertificatesController');
    Route::resource('dubai-services', 'DubaiServicesController');
    Route::resource('partners', 'PartnersController');
    Route::resource('newsletters', 'NewslettersController');
    Route::resource('contacts', 'ContactsController');
    Route::resource('hajj-programs', 'HajjProgramsController');
    Route::resource('omra-programs', 'OmraProgramsController');
    Route::resource('hajj-hotels', 'HajjHotelsController');
    Route::resource('omra-hotels', 'OmraHotelsController');
    Route::apiResource('photos', 'PhotosController');
    Route::view('gallery/photos', 'admin.gallery.photos')->name('gallery.photos');
    Route::resource('bookings', 'BookingsController');
    Route::get('settings', 'SettingsController@index')->name('settings.index');
    Route::get('settings/social', 'SettingsController@editSocialMedia')->name('settings.social');
    Route::post('settings/social', 'SettingsController@updateSocialMedia')->name('settings.update-social');
    Route::get('settings/pages-header', 'SettingsController@editPagesHeader')->name('settings.pages-header');
    Route::post('settings/pages-header', 'SettingsController@updatePagesHeader')->name('settings.update-pages-header');
});

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/
Route::namespace('Front')->prefix('{lang?}')->group(function () {
    Route::view('/', 'front.home');
    Route::get('pages/{slug}', 'PagesController@show');
    Route::resource('services', 'ServicesController')->only(['index', 'show']);
    Route::resource('dubai-services', 'DubaiServicesController')->only(['index', 'show']);
    Route::resource('offers', 'OffersController')->only(['index', 'show']);
    Route::get('our-programs', 'ProgramsController@categories');
    Route::get('our-programs/{category_slug}/programs', 'ProgramsController@index');
    Route::get('our-programs/{program_slug}', 'ProgramsController@show');
    Route::resource('certificates', 'CertificatesController')->only(['index']);
    Route::get('contact-us', 'ContactsController@show');
    Route::post('contacts/send', 'ContactsController@send');
    Route::get('jobs', 'JobsController@show');
    Route::post('jobs/send', 'JobsController@send');
    Route::resource('hajj-programs', 'HajjProgramsController')->only(['index', 'show']);
    Route::resource('umra-programs', 'OmraProgramsController')->only(['index', 'show']);
    Route::resource('hajj-hotels', 'HajjHotelsController')->only(['index', 'show']);
    Route::resource('umra-hotels', 'OmraHotelsController')->only(['index', 'show']);
    Route::view('gallery/photos', 'front.gallery.photos');
    Route::resource('newsletters', 'NewslettersController')->only(['index', 'show']);

    Route::resource('events', 'EventsController')->only(['index', 'show']);

    Route::middleware('auth:web')->group(function(){
        Route::get('programs/{slug}/book', 'BookingsController@create')->name('bookings.create');
        Route::post('programs/{slug}/book', 'BookingsController@store')->name('bookings.store');
        Route::get('programs/{slug}/book/response', 'BookingsController@response')->name('bookings.response');
    });
});

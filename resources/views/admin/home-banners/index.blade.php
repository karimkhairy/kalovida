@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Home Banners', 'crumbs'=>[ 
    ['title'=>'Settings', 'url'=>route('admin.settings.index')],
    ['title'=>'Home banners', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-10">
                </div>
                <div class="col-2 text-right">
                    <a href="{{route('admin.home-banners.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                </div>
            </div>

        </div>
    </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Banner</th>
                    <th scope="col">Alt</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($banners as $banner)
                <tr>
                    <td>@thumb(['image'=>$banner->image, 'title'=>$banner->alt_en])</td>
                    <td>{{$banner->alt_en}}</td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.home-banners.edit', $banner->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.home-banners.destroy', $banner->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>


    </div>
</div>
@endsection

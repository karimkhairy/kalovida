@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Home banners', 'crumbs'=>[ 
    ['title'=>'Settings', 'url'=>route('admin.settings.index')],
    ['title'=>'Home banners', 'url'=>route('admin.home-banners.index')], ['title'=>'Create', 'url'=>''] ] ])
@endsection

@section('content')
{{-- content block --}}
<form action="{{route('admin.home-banners.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        {{-- data in english --}}
        <div class="col-6">
            <div class="card">
                <div class="card-header">Data in English</div>
                <div class="card-body">
                    @input(['type'=>'file', 'name'=>'image', 'label'=>'Image'])
                    @input(['name'=>'alt_en', 'label'=>'Alt'])
                    @textarea(['name'=>'description_en', 'label'=>'Description'])
                </div>
            </div>
        </div>
        {{-- data in arabic --}}
        <div class="col-6">
            <div class="card">
                <div class="card-header">Data in Arabic</div>
                <div class="card-body">
                    @input(['name'=>'alt_ar', 'label'=>'Alt'])
                    @textarea(['name'=>'description_ar','label'=>'Description'])
                </div>
            </div>
        </div>
    </div>

    {{-- buttons --}}
    <div class="card">
            <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
    </div>


@endsection

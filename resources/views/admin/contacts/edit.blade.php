@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Contacts', 'crumbs'=>[
['title'=>'Contacts', 'url'=>''] ] ])
@endsection

@section('content')

    {{-- content block --}}
    <form action="{{route('admin.contacts.update', $contact->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            {{-- data in english --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in English</div>
                    <div class="card-body">
                        @input(['name'=>'address_en', 'label'=>'Address', 'value'=>$contact->address_en])
                        @input(['name'=>'email', 'label'=>'E-Mail', 'value'=>$contact->email])
                        @input(['name'=>'phone', 'label'=>'Phone', 'value'=>$contact->phone])
                        @textarea(['name'=>'map_url', 'label'=>'Map url', 'value'=>$contact->map_url])
                    </div>
                </div>
            </div>
            {{-- data in arabic --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in Arabic</div>
                    <div class="card-body">
                            @input(['name'=>'address_ar', 'label'=>'Address', 'value'=>$contact->address_ar])
                    </div>
                </div>
            </div>
        </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
@endsection

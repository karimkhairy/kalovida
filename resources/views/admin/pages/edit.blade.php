@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>$page->title_en, 'crumbs'=>[
['title'=>$page->title_en, 'url'=>''] ] ])
@endsection

@section('content')

    {{-- content block --}}
    <form action="{{route('admin.pages.update', $page->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            {{-- data in english --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in English</div>
                    <div class="card-body">
                        @input(['name'=>'title_en', 'label'=>'Title', 'value'=>$page->title_en])
                        @textarea(['name'=>'description_en', 'label'=>'Description', 'value'=>$page->description_en])
                        @textarea(['name'=>'body_en', 'label'=>'Body', 'value'=>$page->body_en, 'class'=>'ckeditor'])

                        @if(!in_array($page->id, [4,5]))
                        <div class="row">
                            <div class="col-8">
                                @input(['type'=>'file', 'name'=>'image', 'label'=>'Image'])
                            </div>
                            <div class="col-4">
                                @thumb(['image'=>$page->image])
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
            {{-- data in arabic --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in Arabic</div>
                    <div class="card-body">
                            @input(['name'=>'title_ar', 'label'=>'Title', 'value'=>$page->title_ar])
                            @textarea(['name'=>'description_ar', 'label'=>'Description', 'value'=>$page->description_ar])
                            @textarea(['name'=>'body_ar', 'label'=>'Body', 'value'=>$page->body_ar, 'class'=>'ckeditor'])
                    </div>
                </div>
            </div>
        </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
@endsection

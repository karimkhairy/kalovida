@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Hajj Hotels', 'crumbs'=>[ ['title'=>'Hajj Hotels', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    {{-- search block --}}
    <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <form class="form-inline" method="GET">
                            <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    <div class="col-2 text-right">
                        <a href="{{route('admin.hajj-hotels.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                </div>

            </div>
        </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($hotels as $hotel)
                <tr>
                    <th scope="row">{{$hotel->id}}</th>
                    <td>@thumb(['image'=>$hotel->image, 'title'=>$hotel->title_en])</td>
                    <td>{{$hotel->title_en}}</td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.hajj-hotels.edit', $hotel->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.hajj-hotels.destroy', $hotel->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
                {{ $hotels->links() }}
            </div>

    </div>
</div>
@endsection

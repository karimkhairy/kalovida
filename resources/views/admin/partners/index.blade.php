@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Our partners', 'crumbs'=>[ ['title'=>'Our partners', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-10">
                </div>
                <div class="col-2 text-right">
                    <a href="{{route('admin.partners.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                </div>
            </div>

        </div>
    </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Logo</th>
                    <th scope="col">Title</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($partners as $partner)
                <tr>
                    <td>@thumb(['image'=>['original'=>$partner->image, 'thumb'=>$partner->image], 'title'=>$partner->title_en])</td>
                    <td>{{$partner->title_en}}</td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.partners.edit', $partner->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.partners.destroy', $partner->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>


    </div>
</div>
@endsection

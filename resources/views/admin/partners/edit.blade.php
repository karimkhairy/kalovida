@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Our partners', 'crumbs'=>[ ['title'=>'Our partners', 'url'=>route('admin.partners.index')],
['title'=>'Edit', 'url'=>''] ] ])
@endsection

@section('content')

    {{-- content block --}}
    <form action="{{route('admin.partners.update', $partner->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            {{-- data in english --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in English</div>
                    <div class="card-body">
                        @input(['type'=>'file', 'name'=>'image', 'label'=>'Logo'])
                        @input(['name'=>'title_en', 'label'=>'Title', 'value'=>$partner->title_en])
                        @textarea(['name'=>'description_en', 'label'=>'Description', 'value'=>$partner->description_en])
                    </div>
                </div>
            </div>
            {{-- data in arabic --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in Arabic</div>
                    <div class="card-body">
                        @input(['name'=>'title_ar', 'label'=>'Title', 'value'=>$partner->title_ar])
                        @textarea(['name'=>'description_ar','label'=>'Description', 'value'=>$partner->description_ar])
                    </div>
                </div>
            </div>
        </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
@endsection

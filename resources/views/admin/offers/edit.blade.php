@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Offers', 'crumbs'=>[ ['title'=>'Offers', 'url'=>route('admin.offers.index')],
['title'=>'Edit', 'url'=>''] ] ])
@endsection

@section('content')

    {{-- content block --}}
    <form action="{{route('admin.offers.update', $offer->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
                {{-- data in english --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in English</div>
                        <div class="card-body">
                            @input(['name'=>'title_en', 'label'=>'Title', 'value'=>$offer->title_en])
                            @textarea(['name'=>'description_en', 'label'=>'Description', 'value'=>$offer->description_en])
                            @textarea(['name'=>'body_en', 'label'=>'Body', 'value'=>$offer->body_en, 'class'=>'ckeditor'])
                            @input(['type'=>'file', 'name'=>'image', 'label'=>'Image'])
                        </div>
                    </div>
                </div>
                {{-- data in arabic --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in Arabic</div>
                        <div class="card-body">
                                @input(['name'=>'title_ar', 'label'=>'Title', 'value'=>$offer->title_ar])
                                @textarea(['name'=>'description_ar', 'label'=>'Description', 'value'=>$offer->description_ar])
                                @textarea(['name'=>'body_ar', 'label'=>'Body', 'value'=>$offer->body_ar, 'class'=>'ckeditor'])
                        </div>
                    </div>
                </div>
            </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
@endsection

@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Offers', 'crumbs'=>[ ['title'=>'Offers', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    {{-- search block --}}
    <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <form class="form-inline" method="GET">
                            <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    <div class="col-2 text-right">
                        <a href="{{route('admin.offers.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                </div>

            </div>
        </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col" class="text-center">In Home</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($offers as $offer)
                <tr>
                    <th scope="row">{{$offer->id}}</th>
                    <td>@thumb(['image'=>$offer->image, 'title'=>$offer->title_en])</td>
                    <td>{{$offer->title_en}}</td>
                    <td class="text-center">
                            @actionLink(['url'=>route('admin.offers.show-in-home', $offer->id), 'icon'=>$offer->show_in_home?'fa-eye':'fa-eye-slash', 'tooltip'=>$offer->show_in_home?'click to hide from home page':'click to show in home page'])
                        </td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.offers.edit', $offer->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.offers.destroy', $offer->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
                {{ $offers->links() }}
            </div>

    </div>
</div>
@endsection

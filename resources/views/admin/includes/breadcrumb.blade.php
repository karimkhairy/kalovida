<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{$title ?? ''}}</h1>
            </div>
        </div>
    </div>
    @if(!empty($crumbs))
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{url('/admin')}}">Dashboard</a></li>
                    @foreach($crumbs as $crumb)
                    <li><a href="{{$crumb['url']}}">{{$crumb['title']}}</a></li>
                    @endforeach
                </ol>
            </div>
        </div>
    </div>
    @endif
</div>

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false"
                aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ url('admin') }}">Kalovida Admin</a>
            <a class="navbar-brand hidden" href="{{ url('admin') }}">K</a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('admin.dashboard.index') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <li>
                    <a href="{{ route('admin.users.index') }}"> <i class="menu-icon fa fa-users"></i>Users </a>
                </li>

                <li>
                    <a href="{{ route('admin.offers.index') }}"> <i class="menu-icon fa fa-bullhorn"></i>Offers </a>
                </li>

                <li>
                        <a href="{{ route('admin.events.index') }}"> <i class="menu-icon fa fa-calendar-o"></i>Events </a>
                    </li>

                <li>
                    <a href="{{ route('admin.categories.index') }}"> <i class="menu-icon fa fa-tasks"></i>Our programs </a>
                </li>
                

                <li>
                    <a href="{{ route('admin.certificates.index') }}"> <i class="menu-icon fa fa-certificate"></i>Certificates </a>
                </li>

                <li>
                    <a href="{{ route('admin.partners.index') }}"> <i class="menu-icon fa  fa-user-circle-o"></i>Our Partners</a>
                </li>
                <li>
                    <a href="{{ route('admin.newsletters.index') }}"> <i class="menu-icon fa fa-newspaper-o"></i>News</a>
                </li>
                <li>
                    <a href="{{ route('admin.bookings.index') }}"> <i class="menu-icon fa fa-calendar-check-o"></i>Bookings</a>
                </li>


                <h3 class="menu-title">Pages</h3>
                <li>
                    <a href="{{ route('admin.pages.edit', 1) }}"> <i class="menu-icon fa fa-info"></i>About us </a>
                </li>
                <li>
                    <a href="{{ route('admin.pages.edit', 5) }}"> <i class="menu-icon fa fa-paper-plane-o"></i>Our Mission </a>
                </li>
                <li>
                    <a href="{{ route('admin.contacts.edit', 1) }}"> <i class="menu-icon fa fa-volume-control-phone"></i>Contacts</a>
                </li>

                <h3 class="menu-title">Services</h3>
                <li>
                    <a href="{{ route('admin.services.index') }}"> <i class="menu-icon fa fa-briefcase"></i>Services </a>
                </li>
                <li>
                    <a href="{{ route('admin.dubai-services.index') }}"> <i class="menu-icon fa fa-briefcase"></i>Dubai Services </a>
                </li>

                <h3 class="menu-title">Hajj and Omra</h3>
                <!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-plane"></i>Hajj</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-tasks"></i><a href="{{ route('admin.hajj-programs.index') }}">Programs</a></li>
                        <li><i class="fa fa-h-square"></i><a href="{{ route('admin.hajj-hotels.index') }}">Hotels</a></li>
                        <li><i class="fa fa-info"></i><a href="{{ route('admin.pages.edit', 2) }}">Info</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-plane"></i>Omra</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-tasks"></i><a href="{{ route('admin.omra-programs.index') }}">Programs</a></li>
                        <li><i class="fa fa-h-square"></i><a href="{{ route('admin.omra-hotels.index') }}">Hotels</a></li>
                        <li><i class="fa fa-info"></i><a href="{{ route('admin.pages.edit', 3) }}">Info</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-camera"></i>Gallery</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-video-camera"></i><a href="{{ route('admin.gallery.photos') }}">Photos</a></li>
                        <li><i class="fa fa-file-image-o"></i><a href="{{ route('admin.pages.edit', 4) }}">Videos </a></li>
                    </ul>
                </li>


                <li>
                    <a href="{{ route('admin.settings.index') }}"> <i class="menu-icon fa fa-cog"></i>Settings </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</aside>


@php 
if(!isset($k)) { 
    $k = null; 
}

if(!isset($v)) { 
    $v = null; 
}
@endphp

<div id="add-photo-message"></div>

<form id="add-photo">
    @csrf
    <input type="hidden" name="k" value="{{$k}}"/>
    <input type="hidden" name="v" value="{{$v}}"/>
    <div class="row">
        <div class="col-10">@input(['type'=>'file', 'name'=>'image', 'id'=>'image-upload']) </div>
        <div class="col-2 text-right">@button(['type'=>'submit', 'title'=>'Save', 'id'=>'add-photo-btn'])</div>
    </div>
    <div class="row">
        <div class="col-6">@input(['name'=>'alt_en', 'placeholder'=>'Alt (en)', 'id'=>'alt_en-upload']) </div>
        <div class="col-6">@input(['name'=>'alt_ar','placeholder'=>'Alt (ar)', 'id'=>'alt_ar-upload'])</div>
    </div>

</form>

<table class="table" id="photos-table">
    <tr>
        <th>Image</th>
        <th>Alt</th>
        <th></th>
    </tr>
    <tr>

    </tr>
</table>


@push('scripts')
<script>
    var photos = [];

jQuery(document).ready(function($){
    getPhotos();
    $('#add-photo').on('submit', function(e){
        e.preventDefault();
        showLoading();
        var form = $(this);
            $.ajax({
                    url: `{{route("admin.photos.index")}}?k={{$k}}&v={{$v}}`,
                    type: "POST",
                    data:  new FormData(form[0]),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(res){
                        hideLoading();
                        clearForm()
                        photos.push(res);
                        drawPhotoOnDom()
                        showMessage('success', 'Photo uploaded successfully.')
                    },
                    error: function(e){
                        hideLoading()

                        if(e.status === 422){
                            clearForm()
                            showMessage('danger', 'Error in provided data')
                            Object.keys(e.responseJSON.errors).forEach(function(key){
                                $('#'+key+'-upload').after('<div class="invalid-feedback">'+e.responseJSON.errors[key][0]+'</div>')
                                $('#'+key+'-upload').addClass('is-invalid')
                            })

                        }else{
                            showMessage('danger', 'Failed to upload photo, server error')
                        }
                    },
                    });
    })

    window.deletePhoto = function (id){
        if(confirm('Are you sure to delete this photo?')){
            var data = new FormData();
            data.append('_token', document.head.querySelector('meta[name="csrf-token"]').content)
            data.append('_method', 'DELETE')
            $.ajax({
                    url: "{{url('admin/photos')}}/" + id,
                    type: "POST",
                    data:  data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(res){

                        var index = photos.findIndex(p=>p.id == id);
                        if(index>=0){
                            photos.splice(index, 1);
                            drawPhotoOnDom()
                            showMessage('success', 'Photo deleted successfully')
                        }
                    },
                    error: function(e){
                         showMessage('danger', 'Failed to delete photo, server error')

                    }
            });
        }
    }

function getPhotos(){
    url = `{{route('admin.photos.index')}}?k={{$k}}&v={{$v}}`;
    $.getJSON(url, function(data){
        photos= data;
        drawPhotoOnDom();
    })
}

function deletePhoto(id){
    if(confirm('Are you sure to delete this photo?')){
        console.log('delete')
    }
}

function drawPhotoOnDom(){
    var content = `
    <tr>
        <th>Image</th>
        <th>Alt</th>
        <th></th>
    </tr>
    `
    photos.forEach(function(photo){
        content += `
            <tr>
                <td><a href="${photo.image.original}" class="cbox"><img src="${photo.image.thumb}" class="img-thumbnail"></a><td>
                <td>${photo.alt_en || ''}</td>
                <td>
                    <a href="javascript:void(0)" title="Delete" data-toggle="tooltip" onclick="deletePhoto(${photo.id})"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>
                </td>
            </tr>
        `
    })

    $('#photos-table').html(content)
    $(".cbox").colorbox({rel: 'group2', transition: "scale", scalePhotos: true, maxWidth: "70%", maxHeight: "70%"});
}



function showLoading(){
    $('#add-photo-btn').attr("disabled", true);
    $('#add-photo-btn').html('<i class="fa fa-spinner fa-spin"></i>');
}
function hideLoading(){
    $('#add-photo-btn').attr("disabled", false);
    $('#add-photo-btn').html('Save');
}

function clearForm(){
    $('#add-photo').trigger("reset");
    $('#add-photo .invalid-feedback').remove();
    $('#add-photo .is-invalid').removeClass('is-invalid');
}

function showMessage(type, body){
    $('#add-photo-message').html(`
    <div class="alert alert-${type}" role="alert">
    ${body}
    </div>
    `)

    setTimeout(function(){
        $('#add-photo-message').html('')
    }, 10000)
}

});

</script>






@endpush

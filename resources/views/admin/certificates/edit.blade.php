@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Certificates', 'crumbs'=>[ ['title'=>'Certificates', 'url'=>route('admin.certificates.index')],
['title'=>'Edit', 'url'=>''] ] ])
@endsection

@section('content')

    {{-- content block --}}
    <form action="{{route('admin.certificates.update', $certificate->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            {{-- data in english --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in English</div>
                    <div class="card-body">
                        @input(['type'=>'file', 'name'=>'image', 'label'=>'Image'])
                        @input(['name'=>'title_en', 'label'=>'Title', 'value'=>$certificate->title_en])
                        @textarea(['name'=>'description_en', 'label'=>'Description', 'value'=>$certificate->description_en])
                    </div>
                </div>
            </div>
            {{-- data in arabic --}}
            <div class="col-6">
                <div class="card">
                    <div class="card-header">Data in Arabic</div>
                    <div class="card-body">
                        @input(['name'=>'title_ar', 'label'=>'Title', 'value'=>$certificate->title_ar])
                        @textarea(['name'=>'description_ar','label'=>'Description', 'value'=>$certificate->description_ar])
                    </div>
                </div>
            </div>
        </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
@endsection

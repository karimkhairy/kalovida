@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Certificates', 'crumbs'=>[ ['title'=>'Certificates', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-10">
                </div>
                <div class="col-2 text-right">
                    <a href="{{route('admin.certificates.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                </div>
            </div>

        </div>
    </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Certificate</th>
                    <th scope="col">Title</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($certificates as $certificate)
                <tr>
                    <td>@thumb(['image'=>$certificate->image, 'title'=>$certificate->title_en])</td>
                    <td>{{$certificate->title_en}}</td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.certificates.edit', $certificate->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.certificates.destroy', $certificate->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>


    </div>
</div>
@endsection

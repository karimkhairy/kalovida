@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'News', 'crumbs'=>[ ['title'=>'News', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    {{-- search block --}}
    <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <form class="form-inline" method="GET">
                            <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    <div class="col-2 text-right">
                        <a href="{{route('admin.newsletters.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                </div>

            </div>
        </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($newsletters as $newsletter)
                <tr>
                    <th scope="row">{{$newsletter->id}}</th>
                    <td>@thumb(['image'=>$newsletter->image, 'title'=>$newsletter->title_en])</td>
                    <td>{{$newsletter->title_en}}</td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.newsletters.edit', $newsletter->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.newsletters.destroy', $newsletter->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
                {{ $newsletters->links() }}
            </div>

    </div>
</div>
@endsection

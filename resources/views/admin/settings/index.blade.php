@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Settings', 'crumbs'=>[ ['title'=>'Settings', 'url'=>'']
] ])
@endsection

@section('styles')
<style>
    .atag {
        display: table;
        height: 90px;
        width: 100%;
        border: 1px solid #958e93;
        text-align: center;
    }

    .atag span {
        display: table-cell;
        vertical-align: middle;
    }
</style>
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-2">
                <a href="{{route('admin.home-banners.index')}}" class="atag">
                    <span>Home Banners</span>
                </a>
            </div>
            <div class="col-2">
                <a href="{{route('admin.settings.pages-header')}}" class="atag">
                    <span>Page header image</span>
                </a>
            </div>

            <div class="col-2">
                <a href="{{route('admin.settings.social')}}" class="atag">
                    <span>Social media links</span>
                </a>
            </div>

        </div>
    </div>
</div>
@endsection
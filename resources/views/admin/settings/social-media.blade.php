@extends('admin.layouts.master')
@section('breadcrumb') 
@breadcrumb(
    [ 
        'title'=>'Social media links', 
        'crumbs'=>[
            ['title'=>'Settings', 'url'=>route('admin.settings.index')],
            ['title'=>'Social media links', 'url'=>'']
        ]
    ] 
)
@endsection

@section('content')

{{-- content block --}}
<form action="{{route('admin.settings.update-social')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card">
        <div class="card-header">Edit social media links</div>
        <div class="card-body">
            @input(['name'=>'facebook', 'label'=>'Facebook', 'value'=>$setting['facebook']??''])
            @input(['name'=>'twitter', 'label'=>'Twitter', 'value'=>$setting['twitter']??''])
            @input(['name'=>'instagram', 'label'=>'Instagram', 'value'=>$setting['instagram']??''])
            @input(['name'=>'youtube', 'label'=>'Youtube', 'value'=>$setting['youtube']??''])
        </div>
    </div>

    {{-- buttons --}}
    <div class="card">
        <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
    </div>
    @endsection
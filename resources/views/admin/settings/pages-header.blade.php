@extends('admin.layouts.master')
@section('breadcrumb') 
@breadcrumb(
    [ 
        'title'=>'Pages header', 
        'crumbs'=>[
            ['title'=>'Settings', 'url'=>route('admin.settings.index')],
            ['title'=>'Pages header', 'url'=>'']
        ]
    ] 
)
@endsection

@section('content')

{{-- content block --}}

    <div class="card">
        <div class="card-header">Edit Page headers</div>
        <div class="card-body">
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <th style="vertical-align: middle">About us page</th>
                        <td>
                            @if(!empty($setting['about_us']))
                            @php
                                $image = [
                                    'original'=>asset('storage/pages-header/'.$setting['about_us']),
                                    'thumb'=>asset('storage/pages-header/'.$setting['about_us'])
                                ]
                            @endphp
                            {{-- <img src="{{asset('storage/pages-header/'.$setting['about_us'])}}" height="70" /> --}}
                                @thumb(['image'=>$image, 'height'=>"70"])
                            @else
                                No current image
                            @endif
                        </td>
                        <td>
                            <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                @csrf
                                @input(['type'=>'hidden', 'name'=>'page', 'value'=>'about_us'])
                                @input(['type'=>'file', 'name'=>'image'])
                                @button(['title'=>'Save', 'icon'=>'floppy-o'])
                            </form>
                        </td>
                    </tr>

                    <tr>
                            <th style="vertical-align: middle">Services page</th>
                            <td>
                                @if(!empty($setting['services']))
                                @php
                                    $image = [
                                        'original'=>asset('storage/pages-header/'.$setting['services']),
                                        'thumb'=>asset('storage/pages-header/'.$setting['services'])
                                    ]
                                @endphp
                                    @thumb(['image'=>$image, 'height'=>"70"])
                                @else
                                    No current image
                                @endif
                            </td>
                            <td>
                                <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                    @csrf
                                    @input(['type'=>'hidden', 'name'=>'page', 'value'=>'services'])
                                    @input(['type'=>'file', 'name'=>'image'])
                                    @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                </form>
                            </td>
                        </tr>

                        <tr>
                                <th style="vertical-align: middle">Offers page</th>
                                <td>
                                    @if(!empty($setting['offers']))
                                    @php
                                        $image = [
                                            'original'=>asset('storage/pages-header/'.$setting['offers']),
                                            'thumb'=>asset('storage/pages-header/'.$setting['offers'])
                                        ]
                                    @endphp
                                        @thumb(['image'=>$image, 'height'=>"70"])
                                    @else
                                        No current image
                                    @endif
                                </td>
                                <td>
                                    <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                        @csrf
                                        @input(['type'=>'hidden', 'name'=>'page', 'value'=>'offers'])
                                        @input(['type'=>'file', 'name'=>'image'])
                                        @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                    <th style="vertical-align: middle">Our Programs page</th>
                                    <td>
                                        @if(!empty($setting['programs']))
                                        @php
                                            $image = [
                                                'original'=>asset('storage/pages-header/'.$setting['programs']),
                                                'thumb'=>asset('storage/pages-header/'.$setting['programs'])
                                            ]
                                        @endphp
                                            @thumb(['image'=>$image, 'height'=>"70"])
                                        @else
                                            No current image
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                            @csrf
                                            @input(['type'=>'hidden', 'name'=>'page', 'value'=>'programs'])
                                            @input(['type'=>'file', 'name'=>'image'])
                                            @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                        <th style="vertical-align: middle">Hajj page</th>
                                        <td>
                                            @if(!empty($setting['hajj']))
                                            @php
                                                $image = [
                                                    'original'=>asset('storage/pages-header/'.$setting['hajj']),
                                                    'thumb'=>asset('storage/pages-header/'.$setting['hajj'])
                                                ]
                                            @endphp
                                                @thumb(['image'=>$image, 'height'=>"70"])
                                            @else
                                                No current image
                                            @endif
                                        </td>
                                        <td>
                                            <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                @csrf
                                                @input(['type'=>'hidden', 'name'=>'page', 'value'=>'hajj'])
                                                @input(['type'=>'file', 'name'=>'image'])
                                                @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                            </form>
                                        </td>
                                    </tr>

                                    <tr>
                                            <th style="vertical-align: middle">Umrah page</th>
                                            <td>
                                                @if(!empty($setting['umrah']))
                                                @php
                                                    $image = [
                                                        'original'=>asset('storage/pages-header/'.$setting['umrah']),
                                                        'thumb'=>asset('storage/pages-header/'.$setting['umrah'])
                                                    ]
                                                @endphp
                                                    @thumb(['image'=>$image, 'height'=>"70"])
                                                @else
                                                    No current image
                                                @endif
                                            </td>
                                            <td>
                                                <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                    @csrf
                                                    @input(['type'=>'hidden', 'name'=>'page', 'value'=>'umrah'])
                                                    @input(['type'=>'file', 'name'=>'image'])
                                                    @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                </form>
                                            </td>
                                        </tr>

                                        <tr>
                                                <th style="vertical-align: middle">Hajj & Umarah gallery page</th>
                                                <td>
                                                    @if(!empty($setting['gallery']))
                                                    @php
                                                        $image = [
                                                            'original'=>asset('storage/pages-header/'.$setting['gallery']),
                                                            'thumb'=>asset('storage/pages-header/'.$setting['gallery'])
                                                        ]
                                                    @endphp
                                                        @thumb(['image'=>$image, 'height'=>"70"])
                                                    @else
                                                        No current image
                                                    @endif
                                                </td>
                                                <td>
                                                    <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                        @csrf
                                                        @input(['type'=>'hidden', 'name'=>'page', 'value'=>'gallery'])
                                                        @input(['type'=>'file', 'name'=>'image'])
                                                        @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                    </form>
                                                </td>
                                            </tr>

                                        <tr>
                                                <th style="vertical-align: middle">Certificates page</th>
                                                <td>
                                                    @if(!empty($setting['certificates']))
                                                    @php
                                                        $image = [
                                                            'original'=>asset('storage/pages-header/'.$setting['certificates']),
                                                            'thumb'=>asset('storage/pages-header/'.$setting['certificates'])
                                                        ]
                                                    @endphp
                                                        @thumb(['image'=>$image, 'height'=>"70"])
                                                    @else
                                                        No current image
                                                    @endif
                                                </td>
                                                <td>
                                                    <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                        @csrf
                                                        @input(['type'=>'hidden', 'name'=>'page', 'value'=>'certificates'])
                                                        @input(['type'=>'file', 'name'=>'image'])
                                                        @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                    </form>
                                                </td>
                                            </tr>

                                            <tr>
                                                    <th style="vertical-align: middle">Contact us page</th>
                                                    <td>
                                                        @if(!empty($setting['contact_us']))
                                                        @php
                                                            $image = [
                                                                'original'=>asset('storage/pages-header/'.$setting['contact_us']),
                                                                'thumb'=>asset('storage/pages-header/'.$setting['contact_us'])
                                                            ]
                                                        @endphp
                                                            @thumb(['image'=>$image, 'height'=>"70"])
                                                        @else
                                                            No current image
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                            @csrf
                                                            @input(['type'=>'hidden', 'name'=>'page', 'value'=>'contact_us'])
                                                            @input(['type'=>'file', 'name'=>'image'])
                                                            @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                        </form>
                                                    </td>
                                                </tr>

                                                <tr>
                                                        <th style="vertical-align: middle">Dubai Services page</th>
                                                        <td>
                                                            @if(!empty($setting['dubai_services']))
                                                            @php
                                                                $image = [
                                                                    'original'=>asset('storage/pages-header/'.$setting['dubai_services']),
                                                                    'thumb'=>asset('storage/pages-header/'.$setting['dubai_services'])
                                                                ]
                                                            @endphp
                                                                @thumb(['image'=>$image, 'height'=>"70"])
                                                            @else
                                                                No current image
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                                @csrf
                                                                @input(['type'=>'hidden', 'name'=>'page', 'value'=>'dubai_services'])
                                                                @input(['type'=>'file', 'name'=>'image'])
                                                                @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                            </form>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                            <th style="vertical-align: middle">Booking page</th>
                                                            <td>
                                                                @if(!empty($setting['booking']))
                                                                @php
                                                                    $image = [
                                                                        'original'=>asset('storage/pages-header/'.$setting['booking']),
                                                                        'thumb'=>asset('storage/pages-header/'.$setting['booking'])
                                                                    ]
                                                                @endphp
                                                                    @thumb(['image'=>$image, 'height'=>"70"])
                                                                @else
                                                                    No current image
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                                    @csrf
                                                                    @input(['type'=>'hidden', 'name'=>'page', 'value'=>'booking'])
                                                                    @input(['type'=>'file', 'name'=>'image'])
                                                                    @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                                </form>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                                <th style="vertical-align: middle">News page</th>
                                                                <td>
                                                                    @if(!empty($setting['news']))
                                                                    @php
                                                                        $image = [
                                                                            'original'=>asset('storage/pages-header/'.$setting['news']),
                                                                            'thumb'=>asset('storage/pages-header/'.$setting['news'])
                                                                        ]
                                                                    @endphp
                                                                        @thumb(['image'=>$image, 'height'=>"70"])
                                                                    @else
                                                                        No current image
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                                        @csrf
                                                                        @input(['type'=>'hidden', 'name'=>'page', 'value'=>'news'])
                                                                        @input(['type'=>'file', 'name'=>'image'])
                                                                        @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                                    </form>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                    <th style="vertical-align: middle">Jobs page</th>
                                                                    <td>
                                                                        @if(!empty($setting['jobs']))
                                                                        @php
                                                                            $image = [
                                                                                'original'=>asset('storage/pages-header/'.$setting['jobs']),
                                                                                'thumb'=>asset('storage/pages-header/'.$setting['jobs'])
                                                                            ]
                                                                        @endphp
                                                                            @thumb(['image'=>$image, 'height'=>"70"])
                                                                        @else
                                                                            No current image
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                                            @csrf
                                                                            @input(['type'=>'hidden', 'name'=>'page', 'value'=>'jobs'])
                                                                            @input(['type'=>'file', 'name'=>'image'])
                                                                            @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                                        </form>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                        <th style="vertical-align: middle">Events page</th>
                                                                        <td>
                                                                            @if(!empty($setting['events']))
                                                                            @php
                                                                                $image = [
                                                                                    'original'=>asset('storage/pages-header/'.$setting['events']),
                                                                                    'thumb'=>asset('storage/pages-header/'.$setting['events'])
                                                                                ]
                                                                            @endphp
                                                                                @thumb(['image'=>$image, 'height'=>"70"])
                                                                            @else
                                                                                No current image
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <form action="{{route('admin.settings.update-pages-header')}}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                                                @csrf
                                                                                @input(['type'=>'hidden', 'name'=>'page', 'value'=>'events'])
                                                                                @input(['type'=>'file', 'name'=>'image'])
                                                                                @button(['title'=>'Save', 'icon'=>'floppy-o'])
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                </tbody>
            </table>
            
        </div>
    </div>

   
    @endsection
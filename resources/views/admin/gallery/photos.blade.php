@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Photos', 'crumbs'=>[ ['title'=>'Gallery', 'url'=>''], ['title'=>'Photos', 'url'=>'']
] ])
@endsection

@section('content')


    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        @include('admin.includes.photos')
    </div>
</div>
@endsection

@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Programs categories', 'crumbs'=>[ ['title'=>'Programs categories', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <form class="form-inline" method="GET">
                            <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    <div class="col-2 text-right">
                        <a href="{{route('admin.categories.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                </div>

            </div>
        </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <th scope="row">{{$category->id}}</th>
                    <td>@thumb(['image'=>$category->image, 'title'=>$category->title_en])</td>
                    <td><a href="{{route('admin.programs.index', ['category_id'=>$category->id])}}">{{$category->title_en}}</a></td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.programs.index', ['category_id'=>$category->id]), 'icon'=>'fa-list fa-2x', 'tooltip'=>'Programs list'])
                        @actionLink(['url'=>route('admin.categories.edit', $category->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.categories.destroy', $category->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
                {{ $categories->links() }}
            </div>

    </div>
</div>
@endsection

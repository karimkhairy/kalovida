@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Programs categories', 'crumbs'=>[ ['title'=>'Programs categories', 'url'=>route('admin.categories.index')],
['title'=>'Edit', 'url'=>''] ] ])
@endsection

@section('content')

    {{-- content block --}}
    <form action="{{route('admin.categories.update', $category->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
                {{-- data in english --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in English</div>
                        <div class="card-body">
                            @input(['name'=>'title_en', 'label'=>'Title', 'value'=>$category->title_en])
                            @input(['type'=>'file', 'name'=>'image', 'label'=>'Image'])
                        </div>
                    </div>
                </div>
                {{-- data in arabic --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in Arabic</div>
                        <div class="card-body">
                                @input(['name'=>'title_ar', 'label'=>'Title', 'value'=>$category->title_ar])
                        </div>
                    </div>
                </div>
            </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
@endsection

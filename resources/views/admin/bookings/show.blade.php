@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Bookings', 'crumbs'=>[ ['title'=>'Bookings', 'url'=>route('admin.bookings.index')],
['title'=>'Show', 'url'=>''] ] ])
@endsection

@section('content')

  
        <div class="row">
                {{-- data in english --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Booking info</div>
                        <div class="card-body">
                            @input(['name'=>'title_en', 'label'=>'ID', 'value'=>$booking->id, 'disabled'=>true])
                            @input(['name'=>'created_at', 'label'=>'Created at', 'value'=>$booking->created_at, 'disabled'=>true])
                            
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">User info</div>
                        <div class="card-body">
                            @input(['name'=>'title_en', 'label'=>'ID', 'value'=>$booking->user->id, 'disabled'=>true])      
                            @input(['name'=>'title_en', 'label'=>'Name', 'value'=>$booking->user->name, 'disabled'=>true])      
                            @input(['name'=>'title_en', 'label'=>'E-Mail', 'value'=>$booking->user->email, 'disabled'=>true])      
                        </div>
                    </div>
                </div>
                {{-- data in arabic --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Program info</div>
                        <div class="card-body">
                            @input(['name'=>'title_en', 'label'=>'ID', 'value'=>$booking->program->id, 'disabled'=>true])      
                            @input(['name'=>'title_en', 'label'=>'Title', 'value'=>$booking->program->title_en, 'disabled'=>true])      
                            @textarea(['name'=>'title_en', 'label'=>'Description', 'value'=>$booking->program->description_en, 'disabled'=>true])      
                            @input(['name'=>'title_en', 'label'=>'Category', 'value'=>$booking->program->section, 'disabled'=>true])      
                            @input(['name'=>'title_en', 'label'=>'Price', 'value'=>$booking->program->price, 'disabled'=>true])      
                        </div>
                    </div>
                </div>

                
            </div>

        
@endsection

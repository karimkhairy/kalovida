@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Bookings', 'crumbs'=>[ ['title'=>'Bookings', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    {{-- search block --}}
    <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form class="form-inline" method="GET">
                            <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    
                </div>

            </div>
        </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Program</th>
                    <th scope="col">Price</th>
                    <th scope="col">Created at</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bookings as $booking)
                <tr>
                    <th scope="row">{{$booking->id}}</th>
                    <td>{{$booking->user->name}}</td>
                    <td>{{$booking->program->title_en}}</td>
                    <td>{{$booking->program->price}}</td>
                    <td>{{$booking->created_at}}</td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.bookings.show', $booking->id), 'icon'=>'fa-eye fa-2x', 'tooltip'=>'Show'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
                {{ $bookings->links() }}
            </div>

    </div>
</div>
@endsection

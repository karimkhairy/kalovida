@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Users', 'crumbs'=>[ ['title'=>'Users', 'url'=>route('admin.users.index')],
['title'=>'Edit', 'url'=>''] ] ])
@endsection

@section('content')
<div class="card">


    {{-- content block --}}
    <div class="card-body">
        <form action="{{route('admin.users.update', $user->id)}}" method="POST">
            @csrf
            @method('PUT')
            @select(['name'=>'role', 'label'=>'Role', 'items'=>['user'=>'User', 'admin'=>'Admin'], 'value'=>$user->role])
            @input(['name'=>'name', 'label'=>'Name', 'value'=>$user->name])
            @input(['name'=>'email', 'label'=>'E-Mail', 'value'=>$user->email])
            @input(['name'=>'password', 'label'=>'Password', 'type'=>'password'])
            @input(['name'=>'password_confirmation', 'label'=>'Confirm Password', 'type'=>'password'])
            @button(['title'=>'Save', 'icon'=>'floppy-o'])
        </form>
    </div>
</div>
@endsection

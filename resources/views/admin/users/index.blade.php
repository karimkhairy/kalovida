@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Users', 'crumbs'=>[ ['title'=>'Users', 'url'=>route('admin.users.index')]
] ])
@endsection

@section('content')

    {{-- search block --}}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-10">
                    <form class="form-inline" method="GET">
                        <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                        <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                    </form>
                </div>
                <div class="col-2 text-right">
                    <a href="{{route('admin.users.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                </div>
            </div>

        </div>
    </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Emal</th>
                    <th scope="col">Role</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->role}}</td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.users.edit', $user->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.users.destroy', $user->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
            {{ $users->links() }}
        </div>

    </div>
</div>
@endsection

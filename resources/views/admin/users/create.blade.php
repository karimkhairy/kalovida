@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Users', 'crumbs'=>[ ['title'=>'Users', 'url'=>route('admin.users.index')],
['title'=>'Create', 'url'=>route('admin.users.create')] ] ])
@endsection

@section('content')
<div class="card">


    {{-- content block --}}
    <div class="card-body">
        <form action="{{route('admin.users.store')}}" method="POST">
            @csrf
            @select(['name'=>'role', 'label'=>'Role', 'items'=>['user'=>'User', 'admin'=>'Admin']])
            @input(['name'=>'name', 'label'=>'Name'])
            @input(['name'=>'email', 'label'=>'E-Mail'])
            @input(['name'=>'password', 'label'=>'Password', 'type'=>'password'])
            @input(['name'=>'password_confirmation', 'label'=>'Confirm Password', 'type'=>'password'])
            @button(['title'=>'Save', 'icon'=>'floppy-o'])
        </form>
    </div>
</div>
@endsection

@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Our Programs', 'crumbs'=>[ 
    ['title'=>'Programs categories', 'url'=>route('admin.categories.index')],
    ['title'=>$program->category->title_en, 'url'=>route('admin.programs.index', ['category_id'=>$program->category_id])],

['title'=>'Edit', 'url'=>''] ] ])
@endsection

@section('content')

    {{-- content block --}}
    <form action="{{route('admin.programs.update', $program->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
                {{-- data in english --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in English</div>
                        <div class="card-body">
                            @input(['name'=>'title_en', 'label'=>'Title', 'value'=>$program->title_en])
                            @input(['name'=>'time_en', 'label'=>'Period', 'value'=>$program->time_en])
                            @input(['name'=>'price', 'label'=>'Price', 'value'=>$program->price])
                            @textarea(['name'=>'description_en', 'label'=>'Description', 'value'=>$program->description_en])
                            @textarea(['name'=>'body_en', 'label'=>'Body', 'value'=>$program->body_en, 'class'=>'ckeditor'])
                            @input(['type'=>'file', 'name'=>'image', 'label'=>'Image'])
                        </div>
                    </div>
                </div>
                {{-- data in arabic --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in Arabic</div>
                        <div class="card-body">
                                @input(['name'=>'title_ar', 'label'=>'Title', 'value'=>$program->title_ar])
                                @input(['name'=>'time_ar', 'label'=>'Period', 'value'=>$program->time_ar])
                                @textarea(['name'=>'description_ar', 'label'=>'Description', 'value'=>$program->description_ar])
                                @textarea(['name'=>'body_ar', 'label'=>'Body', 'value'=>$program->body_ar, 'class'=>'ckeditor'])
                        </div>
                    </div>
                </div>
            </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
@endsection

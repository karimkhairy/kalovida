@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Our Programs', 'crumbs'=>[ 
    ['title'=>'Programs categories', 'url'=>route('admin.categories.index')],
    ['title'=>$category->title_en, 'url'=>''],
    ['title'=>'Our Programs', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <form class="form-inline" method="GET">
                            <input type="hidden" name="category_id" value="{{$category->id}}" />
                            <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    <div class="col-2 text-right">
                        <a href="{{route('admin.programs.create', ['category_id'=>$category->id])}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                </div>

            </div>
        </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Time</th>
                    <th scope="col">Price</th>
                    <th scope="col" class="text-center">In Home</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($programs as $program)
                <tr>
                    <th scope="row">{{$program->id}}</th>
                    <td>@thumb(['image'=>$program->image, 'title'=>$program->title_en])</td>
                    <td>{{$program->title_en}}</td>
                    <td>{{$program->time_en}}</td>
                    <td>{{$program->price}}</td>
                    <td class="text-center">
                        @actionLink(['url'=>route('admin.programs.show-in-home', $program->id), 'icon'=>$program->show_in_home?'fa-eye':'fa-eye-slash', 'tooltip'=>$program->show_in_home?'click to hide from home page':'click to show in home page'])
                    </td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.programs.edit', $program->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.programs.destroy', $program->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
                {{ $programs->links() }}
            </div>

    </div>
</div>
@endsection

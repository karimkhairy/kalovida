<form method="POST" action={{$url}} class="form-inline" style="display:inline">
    @csrf
    @method('DELETE')
    @button(['class'=>'link', 'icon'=>$icon??null, 'title'=>$title??null, 'tooltip'=>$tooltip??'', 'onclick'=>"return confirm('".$confirm."')"])
</form>

@php
    if(!isset($value)){
        $value = '';
    }

    if(old($name)){
        $value = old($name);
    }

    if(!isset($type)){
        $type = 'text';
    }
@endphp
<div class="form-group">
    @if(isset($label))
        <label for="{{$id??$name}}">{{$label}}</label>
    @endif

<input type="{{$type}}" name="{{$name}}" id="{{$id??$name}}" class="form-control{{$type==='file'?'-file':''}} {{$errors->has($name)?'is-invalid':null}}" placeholder="{{$placeholder??''}}" value="{{$value}}" {{!empty($disabled)?'disabled':''}}>
    @if(isset($help))
        <small class="form-text text-muted">{{$help}}</small>
    @endif
    @if($errors->has($name))
        <div class="invalid-feedback">{{$errors->first($name)}}</div>
    @endif
</div>

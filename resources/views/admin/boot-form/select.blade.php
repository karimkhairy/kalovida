@php
    if(!isset($value)){
        $value = '';
    }

    if(old($name)){
        $value = old($name);
    }
@endphp

<div class="form-group">
    @if(isset($label))
        <label for="{{$id??$name}}">{{$label}}</label>
    @endif

    <select name="{{$name}}" id="{{$id??$name}}" class="form-control {{$errors->has($name)?'is-invalid':null}}">
        @if(isset($placeholder))
            <option>{{$placeholder}}</option>
        @endif
        @foreach($items as $key => $item)
            <option value="{{$key}}" {{$value == $key? 'selected': null}}>{{$item}}</option>
        @endforeach
    </select>

        @if(isset($help))
            <small class="form-text text-muted">{{$help}}</small>
        @endif
        @if($errors->has($name))
            <div class="invalid-feedback">{{$errors->first($name)}}</div>
        @endif
    </div>

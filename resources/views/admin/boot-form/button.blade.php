@php
$tooltip = $tooltip??false;
@endphp

<button type="{{$type??'submit'}}" class="btn btn-{{$class??'primary'}}" title="{{$tooltip??''}}" {{$tooltip?'data-toggle=tooltip':null}} onclick="{{$onclick??''}}">
    @if(isset($icon))
        <i class="fa fa-{{$icon}}" aria-hidden="true"></i>
    @endif
    {{$title}}
</button>

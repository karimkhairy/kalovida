@php
    if(!isset($value)){
        $value = '';
    }

    if(old($name)){
        $value = old($name);
    }
@endphp
<div class="form-group">
    @if(isset($label))
        <label for="{{$id??$name}}">{{$label}}</label>
    @endif


<textarea class="form-control {{$errors->has($name)?'is-invalid':null}} {{$class??''}}"  name="{{$name}}" id="{{$id??$name}}" rows="{{$rows??3}}" placeholder="{{$placeholder??''}}" {{!empty($disabled)?'disabled':''}}>{{$value}}</textarea>

    @if(isset($help))
        <small class="form-text text-muted">{{$help}}</small>
    @endif
    @if($errors->has($name))
        <div class="invalid-feedback">{{$errors->first($name)}}</div>
    @endif
</div>

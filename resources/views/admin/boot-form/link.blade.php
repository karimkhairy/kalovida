@php
$tooltip = $tooltip??false;
@endphp
<a href="{{$url??'#'}}" class="{{$class??''}}" title="{{$tooltip??''}}" {{$tooltip?'data-toggle=tooltip':null}}>
    @if(isset($icon))
        <i class="fa {{$icon}}" aria-hidden="true"></i>
    @endif
    {{$text??''}}
</a>

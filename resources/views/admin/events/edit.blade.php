@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Events', 'crumbs'=>[ ['title'=>'Events', 'url'=>route('admin.events.index')],
['title'=>'Edit', 'url'=>''] ] ])
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <a href="#" class="btn btn-default" data-toggle="modal" data-target="#event-images"><i class="fa fa-picture-o"></i> Event Gallery</a>
    </div>

    </div>
</div>

    {{-- content block --}}
    <form action="{{route('admin.events.update', $event->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
                {{-- data in english --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in English</div>
                        <div class="card-body">
                            @input(['name'=>'title_en', 'label'=>'Name', 'value'=>$event->title_en])
                            @textarea(['name'=>'description_en', 'label'=>'Description', 'value'=>$event->description_en])
                            @textarea(['name'=>'body_en', 'label'=>'Details', 'value'=>$event->body_en, 'class'=>'ckeditor'])
                            @input(['type'=>'file', 'name'=>'image', 'label'=>'Image'])
                        </div>
                    </div>
                </div>
                {{-- data in arabic --}}
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Data in Arabic</div>
                        <div class="card-body">
                                @input(['name'=>'title_ar', 'label'=>'Name', 'value'=>$event->title_ar])
                                @textarea(['name'=>'description_ar', 'label'=>'Description', 'value'=>$event->description_ar])
                                @textarea(['name'=>'body_ar', 'label'=>'Details', 'value'=>$event->body_ar, 'class'=>'ckeditor'])
                        </div>
                    </div>
                </div>
            </div>

        {{-- buttons --}}
        <div class="card">
                <div class="card-body">@button(['title'=>'Save', 'icon'=>'floppy-o'])</div>
        </div>
    </form>

    <!-- Hotel images modal -->
<div class="modal fade" id="event-images" tabindex="-1" role="dialog" aria-labelledby="event-imagesLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="event-imagesLabel">Hotel Images</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @include('admin.includes.photos', ['k'=>'event_id', 'v'=>$event->id])
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection

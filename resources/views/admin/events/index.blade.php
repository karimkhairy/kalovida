@extends('admin.layouts.master')
@section('breadcrumb') @breadcrumb([ 'title'=>'Events', 'crumbs'=>[ ['title'=>'Events', 'url'=>'']
] ])
@endsection

@section('content')

    {{-- search block --}}
    <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <form class="form-inline" method="GET">
                            <input name="key" type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="key.." value="{{request('key', '')}}">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                    <div class="col-2 text-right">
                        <a href="{{route('admin.events.create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                </div>

            </div>
        </div>

    {{-- content block --}}
    <div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Name</th>
                    <th scope="col" class="text-center">In Home</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($events as $event)
                <tr>
                    <th scope="row">{{$event->id}}</th>
                    <td>@thumb(['image'=>$event->image, 'title'=>$event->title_en])</td>
                    <td>{{$event->title_en}}</td>
                    <td class="text-center">
                            @actionLink(['url'=>route('admin.events.show-in-home', $event->id), 'icon'=>$event->show_in_home?'fa-eye':'fa-eye-slash', 'tooltip'=>$event->show_in_home?'click to hide from home page':'click to show in home page'])
                        </td>
                    <td class="text-right">
                        @actionLink(['url'=>route('admin.events.edit', $event->id), 'icon'=>'fa-pencil-square-o fa-2x', 'tooltip'=>'Edit'])
                        @deleteLink(['url'=>route('admin.events.destroy', $event->id), 'icon'=>'trash fa-2x', 'tooltip'=>'Delete', 'confirm'=>'Are you sure to delete?'])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right">
                {{ $events->links() }}
            </div>

    </div>
</div>
@endsection

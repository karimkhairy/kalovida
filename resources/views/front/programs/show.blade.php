@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>translate($program, 'title'), 'description'=>translate($program,
'description') ])
@endsection

@section('content')
    @include('front.includes.inner-banner', [ 'title'=>translate($program,
'title'), 'image'=>getPageHeaderImage('programs'), 'crumbs'=>[ ['title'=>'Our Programs', 'url'=>i18nUrl('programs')], ['title'=>translate($program,
'title'), 'url'=>''] ] ])

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="blog-list">
                    <div>
                        <div class="blog-list-top">
                            <img class="img-responsive img-full" src="{{$program->image['original']}}" alt="">
                        </div>


                        <div class="blog-list-text">
                            {!! translate($program, 'body') !!}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

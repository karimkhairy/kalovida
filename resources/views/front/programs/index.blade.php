@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Our Programs') ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'Our Programs', 'image'=>getPageHeaderImage('programs'), 'crumbs'=>[ ['title'=>'Our Programs', 'url'=>''] ] ])


<div class="list-wrapper bg-grey-2">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="list-content clearfix">
                        @foreach($programs as $program)
                        <div class="list-item-entry">
                          <div class="hotel-item style-3 bg-white">
                              <div class="table-view">
                                    <div class="radius-top cell-view">
                                    <img src="{{$program->image['thumb']}}" alt="">
                                    </div>
                                    <div class="title hotel-middle clearfix cell-view">
                                         <div class="date list-hidden">{{ translate($program, 'time') }}</div>
                                         <div class="date grid-hidden">{{ translate($program, 'time') }}</div>
                                        <h4><b>{{ translate($program, 'title') }}</b></h4>

                                      <p class="f-14 grid-hidden">{{ translate($program, 'description') }}</p>
                                  </div>
                                  <div class="title hotel-right clearfix cell-view">

                                  <a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="{{i18nUrl('our-programs/'.$program->slug)}}">@lang('view more')</a>
                                  </div>
                              </div>
                          </div>
                        </div>
                        @endforeach

                    </div>

                    {{ $programs->links('front.includes.pagination') }}
                </div>
            </div>
        </div>
    </div>
@endsection

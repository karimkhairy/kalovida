@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Our Programs') ])
@endsection

@section('content')
@include('front.includes.inner-banner',
[ 'title'=>'Our Programs', 'image'=>getPageHeaderImage('programs'), 'crumbs'=>[ ['title'=>'Our Programs', 'url'=>''] ] ])


<div class="main-wraper padd-120">
    <div class="container">
    <div class="blog-grid row">
    @foreach($categories as $category)
    <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
        <div class="s_news-entry style-2">
            <a href="{{i18nUrl('our-programs/'.$category->slug.'/programs')}}"><img class="s_news-img img-responsive" src="{{$category->image['thumb']}}" alt=""></a>
            <h4 class="s_news-title"><a href="{{i18nUrl('our-programs/'.$category->slug.'/programs')}}">{{ translate($category, 'title') }}</a></h4>


            <a href="{{i18nUrl('our-programs/'.$category->slug.'/programs')}}" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>@lang('view more')</span></a>
        </div>
    </div>
    @endforeach

</div>
    </div>
</div>


@endsection




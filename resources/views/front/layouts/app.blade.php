<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no" />

    <link rel="shortcut icon" href="favicon.ico" />
    <link href="{{asset('front/css/en/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/css/en/jquery-ui.structure.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/css/en/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/css/en/style.css')}}" rel="stylesheet" type="text/css" />
    <title>Kalovida Travel</title>
</head>

<body data-color="theme-1">
    <!-- LOADER -->
    <div class="loading dr-blue-2">
        <div class="loading-center">
            <div class="loading-center-absolute">
                <div class="object object_four"></div>
                <div class="object object_three"></div>
                <div class="object object_two"></div>
                <div class="object object_one"></div>
            </div>
        </div>
    </div>
    <img class="center-image" src="{{asset('front/img/bg-1.jpg')}}" alt="">

    <div class="container">
        @yield('content')
        <div class="full-copy">© 2019 All rights reserved.Kalovida Travel</div>
    </div>

    <script src="{{asset('front/js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('front/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('front/js/idangerous.swiper.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.viewportchecker.min.js')}}"></script>
    <script src="{{asset('front/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.mousewheel.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.circliful.min.js')}}"></script>
    <script src="{{asset('front/js/all.js')}}"></script>
</body>


</html>

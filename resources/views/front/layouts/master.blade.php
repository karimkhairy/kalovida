<?php
//dd(request()->is('*pages/about-us'))
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no" />
    @yield('meta')
    <link rel="shortcut icon" href="favicon.ico" />
    <link href="{{asset('front/css/'.app()->getLocale().'/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/css/'.app()->getLocale().'/jquery-ui.structure.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/css/'.app()->getLocale().'/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/css/'.app()->getLocale().'/style.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css">
</head>

<body data-color="theme-1">

    <div class="loading">
        <div class="loading-center">
            <div class="loading-center-absolute">
                <div class="object object_four"></div>
                <div class="object object_three"></div>
                <div class="object object_two"></div>
                <div class="object object_one"></div>
            </div>
        </div>
    </div>

    <header class="color-1 hovered menu-3">
        @include('front.includes.header')
    </header>



    @yield('content')

    <footer class="bg-dark type-2">
        @include('front.includes.footer')
    </footer>

    <script src="{{asset('front/js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('front/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('front/js/idangerous.swiper.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.viewportchecker.min.js')}}"></script>
    <script src="{{asset('front/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.mousewheel.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.circliful.min.js')}}"></script>
    <script src="{{asset('front/js/all.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</body>


</html>

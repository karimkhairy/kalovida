@extends('front.layouts.master')
@section('meta')
@meta()
@endsection

@section('content')
<div class="top-baner swiper-animate arrows">
    @include('front.includes.top-banner')
</div>

@include('front.includes.best-hajj-umra-tours-widget')
@include('front.includes.offers-widget')
@include('front.includes.programs-widget')
@include('front.includes.events-widget')
@include('front.includes.partners-widget')





@endsection

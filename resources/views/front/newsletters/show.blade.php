@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>translate($newsletter, 'title'), 'description'=>translate($newsletter,
'description') ])
@endsection

@section('content')
    @include('front.includes.inner-banner', [ 'title'=>translate($newsletter,
'title'), 'image'=>getPageHeaderImage('news'), 'crumbs'=>[ ['title'=>'Newsletters', 'url'=>i18nUrl('newsletters')], ['title'=>translate($newsletter,
'title'), 'url'=>''] ] ])

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="blog-list">
                    <div>
                        <div class="blog-list-top">
                            <img class="img-responsive img-full" src="{{$newsletter->image['original']}}" alt="">
                        </div>


                        <div class="blog-list-text">
                            {!! translate($newsletter, 'body') !!}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="nav">
                <a href="{{i18nUrl('/')}}" class="logo">
                <img src="{{asset('front/img/logo.png')}}" alt="kalovida travel">
             </a>
                <div class="nav-menu-icon">
                    <a href="#"><i></i></a>
                </div>
                <nav class="menu">
                    <ul>
                        <li class="type-1 {{request()->is('en') || request()->is('ar')?'active':null}}">
                            <a href="{{i18nUrl('/')}}">@lang('Home')<span class="fa fa-angle-down"></span></a></li>
                        <li class="type-1 {{request()->is('*pages/about-us')?'active':null}}"><a href="{{i18nUrl('pages/about-us')}}">@lang('About us')<span class="fa fa-angle-down"></span></a>

                        </li>
                        <li class="type-1 {{request()->is('*services*') && !request()->is('*dubai-services*')?'active':null}}"><a href="{{i18nUrl('services')}}">@lang('Services') <span class="fa fa-chevron-right"></span></a>
                        </li>

                        <li class="type-1 {{request()->is('*offers*')?'active':null}}"><a href="{{i18nUrl('offers')}}">@lang('Offers')<span class="fa fa-angle-down"></span></a></li>

                        <li class="type-1 {{request()->is('*our-programs*')?'active':null}}"><a href="{{i18nUrl('our-programs')}}">@lang('Our Programmes')<span class="fa fa-angle-down"></span></a></li>
                        <li class="type-1 {{request()->is('*hajj-programs*') || request()->is('*umra-programs*') || request()->is('*-info*') || request()->is('*-hotels*') || request()->is('*photos*') || request()->is('*videos*') ?'active':null}}"><a href="#">@lang('Hajj & Umrah')<span class="fa fa-angle-down"></span></a>
                            <ul class="dropmenu">
                                <li><a href="#">@lang('Hajj') <span class="fa fa-chevron-right"></span></a>
                                    <ul class="dropmenu">
                                        <li><a href="{{i18nUrl('hajj-programs')}}">@lang('Programmes')</a></li>
                                        <li><a href="{{i18nUrl('pages/hajj-info')}}">@lang('Info Hajj') </a></li>
                                        <li><a href="{{i18nUrl('hajj-hotels')}}">@lang('Hotels')</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">@lang('Umrah') <span class="fa fa-chevron-right"></span></a>
                                    <ul class="dropmenu">
                                        <li><a href="{{i18nUrl('umra-programs')}}">@lang('Programmes')</a></li>
                                        <li><a href="{{i18nUrl('pages/umra-info')}}">@lang('Info umrah') </a></li>
                                        <li><a href="{{i18nUrl('umra-hotels')}}">@lang('Hotels')</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">@lang('Gallery') <span class="fa fa-chevron-right"></span></a>
                                    <ul class="dropmenu">
                                        <li><a href="{{i18nUrl('gallery/photos')}}">@lang('Photos')</a></li>
                                        <li><a href="{{i18nUrl('pages/videos')}}">@lang('Videos') </a></li>

                                    </ul>
                                </li>



                            </ul>
                        </li>


                        <li class="type-1 {{request()->is('*certificates*')?'active':null}}"><a href="{{i18nUrl('certificates')}}">@lang('Certificates')<span class="fa fa-angle-down"></span></a>

                        </li>

                        <li class="type-1 {{request()->is('*contact-us*')?'active':null}}"><a href="{{i18nUrl('contact-us')}}">@lang('Contact us') <span class="fa fa-chevron-right"></span></a></li>

                        <li class="type-1 {{request()->is('*dubai-services*')?'active':null}}"><a href="{{i18nUrl('dubai-services')}}">@lang('Dubai Services') <span class="fa fa-chevron-right"></span></a>                            </li>
                        <li class="type-1"><a href="#">@lang('book now')<span class="fa fa-chevron-right"></span></a> </li>
                        <li class="type-1">{!! langSwitcherLink() !!}</li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

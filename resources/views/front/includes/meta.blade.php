@php
    if(!isset($title)){
        $title = 'Kalovida Travel';
    }else{
        $title = 'Kalovida Travel - ' . $title;
    }
@endphp

<title>{{$title}}</title>

@if(isset($description))
<meta name="description" content="{{$description}}"/>
@endif

@php
    $events = App\Event::where('show_in_home', true)->get();
@endphp

@if(!$events->isEmpty())
<div class="main-wraper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>@lang('Our Events')</h2>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="top-baner arrows">
                    <div class="swiper-container offers-slider" data-autoplay="5000" data-loop="1" data-speed="500" data-slides-per-view="responsive"
                        data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="3" data-add-slides="3">
                        <div class="swiper-wrapper">
                            @foreach($events as $event)
                            <div class="swiper-slide" data-val="0">
                                <div class="offers-block radius-mask">
                                    <div class="clip">
                                        <div class="bg bg-bg-chrome act" style="background-image:url({{$event->image['original']}})">
                                        </div>
                                    </div>
                                    <div class="tour-layer delay-1"></div>
                                    <div class="vertical-top">
                                        <div class="rate">
                                            <span class="fa fa-star color-yellow"></span>
                                            <span class="fa fa-star color-yellow"></span>
                                            <span class="fa fa-star color-yellow"></span>
                                            <span class="fa fa-star color-yellow"></span>
                                            <span class="fa fa-star color-yellow"></span>
                                        </div>
                                        <h3>{{translate($event, 'title')}}</h3>
                                    </div>
                                    <div class="vertical-bottom">
                                       
                                        <p>{{translate($event, 'description')}}</p>
                                        <a href="#" class="c-button bg-aqua hv-aqua-o b-40"><span>@lang('view more')</span></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                        <div class="pagination  poin-style-1 pagination-hidden"></div>
                    </div>
                    <div class="swiper-arrow-left offers-arrow"><span class="fa fa-angle-left"></span></div>
                    <div class="swiper-arrow-right offers-arrow"><span class="fa fa-angle-right"></span></div>
                </div>
            </div>
        </div>
    </div>
    @endif
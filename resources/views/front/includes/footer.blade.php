<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="footer-block">
                <img src="{{asset('front/img/logo.png')}}" alt="" class="logo-footer">
                <div class="f_text color-grey-7">
                   {{ translate(App\Page::find(5), 'description') }}
                </div>
                <div class="footer-share">
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-google-plus"></span></a>

                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-sm-6 no-padding">
            <div class="footer-block">
                <h6>@lang('kalovida News')</h6>
                @php
                    $newsletters = App\Newsletter::latest()->get()->take(3);
                @endphp
                @foreach($newsletters as $newsletter)
                <div class="f_news clearfix">
                    <a class="f_news-img black-hover" href="#">
                        <img class="img-responsive" src="{{$newsletter->image['thumb']}}" alt="">
                            <div class="tour-layer delay-1"></div>
                        </a>
                    <div class="f_news-content">
                    <a class="f_news-tilte color-white link-red" href="{{i18nUrl('newsletters/'.$newsletter->slug)}}">{{translate($newsletter, 'title')}}</a>
                    <span class="date-f">{{ $newsletter->created_at->format('d/m/Y') }}</span>
                        <a href="{{i18nUrl('newsletters/'.$newsletter->slug)}}" class="r-more">@lang('read more')</a>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="footer-block">
                <h6>Our Pages</h6>
                <ul>
                    <li><a href="{{i18nUrl('jobs')}}" class="color-white link-red">@lang('Job')</a></li>
                    <li> <a href="{{i18nUrl('newsletters')}}" class="color-white link-red">@lang('News')</a></li>
                    <li> <a href="{{i18nUrl('offers')}}" class="color-white link-red">@lang('Offer')</a></li>
                    <li> <a href="{{i18nUrl('events')}}" class="color-white link-red">@lang('Events')</a></li>
                    <li> <a href="{{i18nUrl('services')}}" class="color-white link-red">@lang('Services')</a></li>
                    <li> <a href="{{i18nUrl('contact-us')}}" class="color-white link-red">@lang('Contacts')</a></li>

                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="footer-block">
                @php
                    $contact = App\Contact::first();
                @endphp
                <h6>@lang('Contact Info')</h6>
                <div class="contact-info">
                <div class="contact-line color-grey-3"><i class="fa fa-map-marker"></i><span>{{translate($contact, 'address')}}</span></div>
                    <div class="contact-line color-grey-3"><i class="fa fa-phone"></i><a href="tel:{{$contact->phone}}">{{$contact->phone}}</a></div>
                <div class="contact-line color-grey-3"><i class="fa fa-envelope-o"></i><a href="mailto:{{$contact->email}}">{{$contact->email}}</a></div>


                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-link bg-black">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <span>powered by ITTSOFT 2019</span>
                </div>
                <ul>
                    <li><a class="link-aqua" href="#">@lang('Privacy Policy') </a></li>
                    <li><a class="link-aqua" href="{{i18nUrl('pages/about-us')}}">@lang('About Us')</a></li>
                    <li><a class="link-aqua" href="#">@lang('Support') </a></li>
                    <li><a class="link-aqua" href="#">@lang('FAQ')</a></li>

                </ul>
            </div>
        </div>
    </div>
</div>

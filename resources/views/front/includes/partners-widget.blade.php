@php
    $partners = App\Partner::all();
@endphp

@if(!$partners->isEmpty())
<div class="main-wraper padd-90">
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div class="second-title">

                <h2>@lang('our partners')</h2>

            </div>
        </div>
    </div>
</div>
<div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="1000" data-center="0" data-slides-per-view="responsive"
    data-mob-slides="1" data-xs-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="5" data-add-slides="6">
    <div class="swiper-wrapper">
        @foreach($partners as $partner)
        <div class="swiper-slide">
            <div class="partner-entry">
            <a href="#" title="{{translate($partner, 'title')}}"><img class="img-responsive" src="{{$partner->image}}" alt="{{translate($partner, 'title')}}"></a>
            </div>
        </div>
        @endforeach

    </div>
    <div class="pagination pagination-hidden"></div>
</div>
</div>
@endif

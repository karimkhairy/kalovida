@if ($paginator->hasPages())
<div class="c_pagination clearfix">
        @if ($paginator->onFirstPage())
            <a href="javascript:void(0)" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl disabled">@lang('prev page')</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl">@lang('prev page')</a>
        @endif

        <ul class="cp_content color-3">
                @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </ul>

        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fr">@lang('next page')</a>
        @else
            <a href="javascript:void(0)" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fr">@lang('next page')</a>
        @endif


    </div>

@endif




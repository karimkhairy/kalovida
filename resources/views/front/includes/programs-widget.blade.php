@php
    $programs = App\Program::where('section', 'main')->where('show_in_home', true)->get();
@endphp

@if(!$programs->isEmpty())
<div class="main-wraper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>@lang('Beautiful Trips')</h2>
                        <p class="color-grey">@lang('Curabitur nunc erat, consequat in erat ut, congue bibendum nulla')</p>
                    </div>
                </div>
            </div>
            <div class="row col-no-padd">
                @foreach($programs as $program)
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="photo-block hover-aqua">
                        <div class="tour-layer delay-1"></div>
                    <img src="{{$program->image['thumb']}}" alt="">
                        <div class="vertical-align">
                            <div class="photo-title">
                            <h4 class="delay-1"><b>@lang('Only') <span class="color-aqua">${{$program->price}}</span></b></h4>
                                <a class="hover-it" href="#">
                                <h3>{{translate($program, 'title')}}</h3>
                                </a>
                            <h5 class="delay-1">{{translate($program, 'time')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endif

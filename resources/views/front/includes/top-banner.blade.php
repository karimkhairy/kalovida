@php
    $banners = App\HomeBanner::get();
@endphp


<div class="swiper-container main-slider" data-autoplay="5000" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1">
    <div class="swiper-wrapper">
        @foreach($banners as $banner)
    <div class="swiper-slide " data-val="0">
            <div class="clip">
                <div class="bg bg-bg-chrome act" style="background-image:url({{$banner->image['original']}})">
                </div>
            </div>
            <div class="vertical-align">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <div class="main-title style-3 left">
                                @php
                                    $description = translate($banner, 'description');
                                    $description_lines = explode(PHP_EOL, $description);
                                @endphp

                                <h1>
                                    @foreach($description_lines as $line)
                                        @if ($loop->first)
                                            <span class="color-sea">{{$line}}</span>
                                        @else
                                                <br/>{{$line}}
                                        @endif
                                    @endforeach
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="pagination pagination-hidden poin-style-1"></div>
</div>
<div class="arrow-wrapp m-200">
    <div class="cont-1170">
        <div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
        <div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
    </div>
</div>

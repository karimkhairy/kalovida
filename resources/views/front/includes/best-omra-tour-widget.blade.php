@php
    $oPrograms = App\Program::where('section', 'omra')->where('show_in_home', true)->get();
@endphp

@if(!$oPrograms->isEmpty())
<div class="main-wraper padd-90">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>@lang('The Best Umrah Tours')</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($oPrograms as $oProgram)
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="radius-mask tour-block hover-aqua">
                        <div class="clip">
                            <div class="bg bg-bg-chrome act" style="background-image:url({{$oProgram->image['thumb']}})">
                            </div>
                        </div>
                        <div class="tour-layer delay-1"></div>
                        <div class="tour-caption">
                            <div class="vertical-align">
                                <h3 class="hover-it">@lang('Hajj')</h3>
                                <div class="rate">
                                    <span class="fa fa-star color-yellow"></span>
                                    <span class="fa fa-star color-yellow"></span>
                                    <span class="fa fa-star color-yellow"></span>
                                    <span class="fa fa-star color-yellow"></span>
                                    <span class="fa fa-star color-yellow"></span>
                                </div>
                                <h4>{{translate($oProgram, 'title')}}</h4>
                            </div>
                            <div class="vertical-bottom">

                            <a href="{{i18nUrl('umra-programs/'.$oProgram->slug)}}" class="c-button b-50 bg-aqua hv-transparent fr"><img src="{{asset('front/img/flag_icon.png')}}" alt=""><span>@lang('view more')</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endif

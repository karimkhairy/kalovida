@php
    $offers = App\Offer::where('show_in_home', true)->get();
@endphp

@if(!$offers->isEmpty())
<div class="main-wraper">
        <div class="clip">
            <div class="bg bg-bg-chrome" style="background-image:url({{asset('front/img/tour_slider_bg.jpg')}})">
            </div>
        </div>
        <div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="1000" data-slides-per-view="1" id="tour-slide">
            <div class="swiper-wrapper">
                @foreach($offers as $offer)
                <div class="swiper-slide">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="slider-tour padd-90-90">
                                    {{-- <h3>from $960</h3> --}}
                                    <div class="rate">
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                        <span class="fa fa-star color-yellow"></span>
                                    </div>
                                    <h2>{{translate($offer, 'title')}}</h2>
                                    <h5>{{translate($offer, 'description')}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="pagination poin-style-1"></div>
        </div>
    </div>
@endif

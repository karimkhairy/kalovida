<div class="inner-banner style-6">
        <img class="center-image" src="{{$image}}" alt="">
        <div class="vertical-align">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        @if(isset($crumbs))
                          <ul class="banner-breadcrumb color-white clearfix">
                            <li><a class="link-blue-2" href="{{i18nUrl('/')}}">@lang('Home')</a> /</li>
                              @foreach($crumbs as $crumb)
                          <li><a href="{{$crumb['url']}}">@lang($crumb['title'])</a> {{$loop->last?'':'/'}}</li>
                              @endforeach
                          </ul>
                        @endif
                        <h2 class="color-white">@lang($title)</h2>
                      </div>
                </div>
            </div>
        </div>
    </div>

@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Events') ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'Events', 'image'=>getPageHeaderImage('events'), 'crumbs'=>[ ['title'=>'Events', 'url'=>''] ] ])


<div class="list-wrapper bg-grey-2">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="list-content clearfix">
                       @foreach($events as $event)
                        <div class="list-item-entry">
                          <div class="event-item style-3 bg-white">
                              <div class="table-view">
                                    <div class="radius-top cell-view">
                                         <img src="{{$event->image['thumb']}}" alt="{{ translate($event, 'title') }}">
                                    </div>
                                   <div class="title event-middle clearfix cell-view">


                                      <h4><b>{{ translate($event, 'title') }}</b></h4>

                                      <p class="f-14 grid-hidden">{{ translate($event, 'description') }}</p>
                                  </div>
                                  <div class="title event-right clearfix cell-view">

                                      <a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="{{i18nUrl('events/'.$event->slug)}}">@lang('view more')</a>
                                  </div>
                              </div>
                          </div>
                        </div>
                        @endforeach

                    </div>

                    {{ $events->links('front.includes.pagination') }}
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>'Job' ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'Job', 'image'=>getPageHeaderImage('jobs'), 'crumbs'=>[ ['title'=>'Job', 'url'=>''] ] ])



<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-12">
                    @include('flash::message')
            <form class="simple-from" method="POST" action="{{i18nUrl('jobs/send')}}">
                    @csrf
                    <div class="simple-group">
                        <h3 class="small-title">@lang('Your Personal Information')</h3>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2">@lang('Name')</div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="text" placeholder="@lang('Enter your name')" name="name" value="{{old('name')}}">
                                        @if($errors->has('name'))
                                        <span class="text-danger"><small>{{$errors->first('name')}}</small></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2">@lang('E-mail Address')</div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="text" placeholder="@lang('Enter your e-mail adress')" name="email" value="{{old('email')}}">
                                        @if($errors->has('email'))
                                        <span class="text-danger"><small>{{$errors->first('email')}}</small></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2"> @lang('Address')</div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="text" placeholder="@lang('Enter your adress')" name="address" value="{{old('address')}}">
                                        @if($errors->has('address'))
                                        <span class="text-danger"><small>{{$errors->first('address')}}</small></span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2">@lang('Phone Number')</div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="text" placeholder="@lang('Enter your phone number')" name="phone" value="{{old('phone')}}">
                                        @if($errors->has('phone'))
                                        <span class="text-danger"><small>{{$errors->first('phone')}}</small></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2">@lang('Details')</div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <textarea name="details" rows="5" placeholder="@lang('Enter details')">{{old('details')}}</textarea>
                                            @if($errors->has('details'))
                                            <span class="text-danger"><small>{{$errors->first('details')}}</small></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                        @if(env('GOOGLE_RECAPTCHA_KEY'))
                                                <div class="g-recaptcha"
                                                    data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                                                </div>

                                                @if($errors->has('g-recaptcha-response'))
                                        <span class="text-danger"><small>{{$errors->first('g-recaptcha-response')}}</small></span>
                                        @endif
                                        @endif
                                </div>

                        </div>
                    </div>


                    <input type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="@lang('send')">
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

@extends('front.layouts.master')
@section('meta')
@meta([
        'title'=>trans('Services')
    ])
@endsection


@section('content')
    @include('front.includes.inner-banner', [
        'title'=>'Services',
        'image'=>getPageHeaderImage('services'),
        'crumbs'=>[
            ['title'=>'Service', 'url'=>'']
        ]
    ])
    <div class="main-wraper bg-grey-2 padd-90">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                        <div class="second-title">
                            <h4 class="subtitle color-dr-blue-2 underline">@lang('our services')</h4>
                        </div>
                    </div>
                </div>
                <div class="tour-item-grid row">
                    @foreach($services as $service)
                    <div class="col-mob-12 col-xs-6 col-sm-6 col-md-4">
                        <div class="tour-item style-5">
                            <div class="radius-top">
                            <img src="{{$service->image['thumb']}}" alt="{{translate($service, 'title')}}" width="370" height="201">
                            </div>
                          <div class="tour-desc bg-white">
                            <h4><a class="tour-title color-dark-2 link-dr-blue-2" href="internal.html">{{translate($service, 'title')}}</a></h4>

                              <a href="{{i18nUrl('services/'.$service->slug)}}" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o"><span>@lang('view more')</span></a>
                             </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
@endsection

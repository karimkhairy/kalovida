@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Umrah Hotels') ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'Umrah Hotels', 'image'=>getPageHeaderImage('umrah'), 'crumbs'=>[ ['title'=>'Umrah Hotels', 'url'=>''] ] ])


<div class="list-wrapper bg-grey-2">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="list-content clearfix">
                       @foreach($hotels as $hotel)
                        <div class="list-item-entry">
                          <div class="hotel-item style-3 bg-white">
                              <div class="table-view">
                                    <div class="radius-top cell-view">
                                         <img src="{{$hotel->image['thumb']}}" alt="{{ translate($hotel, 'title') }}">
                                    </div>
                                   <div class="title hotel-middle clearfix cell-view">


                                      <h4><b>{{ translate($hotel, 'title') }}</b></h4>

                                      <p class="f-14 grid-hidden">{{ translate($hotel, 'description') }}</p>
                                  </div>
                                  <div class="title hotel-right clearfix cell-view">

                                      <a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="{{i18nUrl('umra-hotels/'.$hotel->slug)}}">@lang('view more')</a>
                                  </div>
                              </div>
                          </div>
                        </div>
                        @endforeach

                    </div>

                    {{ $hotels->links('front.includes.pagination') }}
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>translate($hotel, 'title'), 'description'=>translate($hotel,
'description') ])
@endsection

@section('content')
    @include('front.includes.inner-banner', [ 'title'=>translate($hotel, 'title'),
'image'=>getPageHeaderImage('hajj'), 'crumbs'=>[ ['title'=>'Hajj Hotels', 'url'=>i18nUrl('umra-hotels')], ['title'=>translate($hotel,
'title'), 'url'=>''] ] ])

<div class="detail-wrapper">
    <div class="container">

        <div class="row padd-90">
            <div class="col-xs-12 col-md-12">
                <div class="detail-content color-1">
                    <div class="detail-top slider-wth-thumbs style-2">
                        <div class="swiper-container thumbnails-preview swiper-swiper-unique-id-0 initialized" data-autoplay="0" data-loop="1" data-speed="500"
                            data-center="0" data-slides-per-view="1" id="swiper-unique-id-0">
                            <div class="swiper-wrapper" style="width: 7980px; transform: translate3d(-1140px, 0px, 0px); transition-duration: 0s; height: 674px;">
                                @foreach($hotel->photos as $photo)
                            <div class="swiper-slide {{$loop->first?'active swiper-slide-visible swiper-slide-active':''}}" data-val="0" style="width: 1140px; height: 674px;">
                                    <img class="img-responsive img-full" src="{{$photo->image['original']}}" alt="{{translate($photo, 'alt')}}">
                                </div>
                                @endforeach
                            </div>
                            <div class="pagination pagination-hidden pagination-swiper-unique-id-0"><span class="swiper-pagination-switch swiper-visible-switch swiper-active-switch"></span>
                                <span
                                    class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span>
                                    <span
                                        class="swiper-pagination-switch"></span>
                            </div>
                        </div>
                        <div class="swiper-container thumbnails swiper-swiper-unique-id-1 initialized pagination-hidden" data-autoplay="0" data-loop="0"
                            data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3" data-sm-slides="5"
                            data-md-slides="5" data-lg-slides="5" data-add-slides="5" id="swiper-unique-id-1">
                            <div class="swiper-wrapper" style="width: 1140px; height: 155px;">
                                    @foreach($hotel->photos as $photo)
                                <div class="swiper-slide {{$loop->first?'current active swiper-slide-visible swiper-slide-active':''}}" data-val="0" style="width: 228px; height: 155px;">
                                    <img class="img-responsive img-full" src="{{$photo->image['thumb']}}" alt="">
                                </div>
                                @endforeach

                            </div>
                            <div class="pagination hidden pagination-swiper-unique-id-1"><span class="swiper-pagination-switch swiper-visible-switch swiper-active-switch" style="display: inline;"></span>
                                <span
                                    class="swiper-pagination-switch swiper-visible-switch" style="display: none;"></span><span class="swiper-pagination-switch swiper-visible-switch" style="display: none;"></span>
                                    <span
                                        class="swiper-pagination-switch swiper-visible-switch" style="display: none;"></span><span class="swiper-pagination-switch swiper-visible-switch" style="display: none;"></span></div>
                        </div>
                    </div>
                    <div class="blog-list-text">{!! translate($hotel, 'body')!!}</div>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection

@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Our Programs') ])
@endsection

@section('content')

@include('front.includes.inner-banner',
[ 'title'=>'Hajj Programs', 'image'=>getPageHeaderImage('hajj'), 'crumbs'=>[ ['title'=>'Hajj Programs', 'url'=>''] ] ])

<div class="main-wraper padd-90">
    	<div class="container">

    		<div class="row">

    		<div class="list-content clearfix">
                    @foreach($programs as $program)
  						<div class="list-item-entry">
					        <div class="hotel-item style-8 bg-white">
					        	<div class="table-view">
						          	<div class="radius-top cell-view">
						          	 	<img src="{{$program->image['thumb']}}" alt="{{ translate($program, 'title') }}">

						          	</div>
						          	<div class="title hotel-middle clearfix cell-view">


						          	    <h4><a href="{{i18nUrl('hajj-programs/'.$program->slug)}}"><b>{{ translate($program, 'title') }}</b></a></h4>
							            <p class="f-14">{{ translate($program, 'description') }}</p>


						            </div>
						            <div class="title hotel-right bg-dr-blue clearfix cell-view">
                                    <div class="hotel-person color-white"> <span>${{$program->price}}</span></div>
                                    <a class="c-button b-40 bg-white color-dark-2 hv-dark-2-o grid-hidden" href="{{i18nUrl('programs/'.$program->slug.'/book')}}">@lang('Book now')</a>
						            </div>
					            </div>
					        </div>
                          </div>
                          @endforeach
  					</div>

                      {{ $programs->links('front.includes.pagination') }}
    		</div>
        </div>

    </div>
@endsection




@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>translate($offer, 'title'), 'description'=>translate($offer,
'description') ])
@endsection

@section('content')
    @include('front.includes.inner-banner', [ 'title'=>translate($offer,
'title'), 'image'=>getPageHeaderImage('offers'), 'crumbs'=>[ ['title'=>'Offers', 'url'=>i18nUrl('offers')], ['title'=>translate($offer,
'title'), 'url'=>''] ] ])

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="blog-list">
                    <div>
                        <div class="blog-list-top">
                            <img class="img-responsive img-full" src="{{$offer->image['original']}}" alt="">
                        </div>


                        <div class="blog-list-text">
                            {!! translate($offer, 'body') !!}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

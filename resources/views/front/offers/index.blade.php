@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Offers') ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'Offers', 'image'=>getPageHeaderImage('offers'), 'crumbs'=>[ ['title'=>'Offers', 'url'=>''] ] ])

<div class="main-wraper padd-120">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">

                </div>
            </div>
        </div>
        <div class="blog-grid row">
            @foreach($offers as $offer)
            <div class="blog-grid-entry col-mob-12 col-xs-12 col-sm-6 col-md-4">
                <div class="s_news-entry style-2">
                    <a href="{{i18nUrl('offers/'.$offer->slug)}}"><img class="s_news-img img-responsive" src="{{$offer->image['thumb']}}" alt=""></a>
                <h4 class="s_news-title"><a href="{{i18nUrl('offers/'.$offer->slug)}}">{{translate($offer, 'title')}}</a></h4>

                <div class="s_news-text color-grey-3">{{translate($offer, 'description')}}</div>
                    <a href="{{i18nUrl('offers/'.$offer->slug)}}" class="c-button small bg-dr-blue-2 hv-dr-blue-2-o"><span>@lang('view more')</span></a>
                </div>
            </div>
            @endforeach

        </div>

        {{ $offers->links('front.includes.pagination') }}


    </div>
</div>
@endsection

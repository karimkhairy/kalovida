@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>'Contact us' ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'Contact us', 'image'=>getPageHeaderImage('contact_us'), 'crumbs'=>[ ['title'=>'Contact us', 'url'=>''] ] ])



<div class="detail-wrapper">
    <!-- CONTACT-FORM -->
    <div class="main-wraper padd-40">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7">
                        @include('flash::message')
                <form class="contact-form js-contact-form" action="{{i18nUrl('contacts/send')}}" method="POST">
                    @csrf
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-style-1 type-2 color-2">
                                    <input type="text" name="name" placeholder="@lang('Enter your name')" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                    <span class="text-danger"><small>{{$errors->first('name')}}</small></span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-style-1 type-2 color-2">
                                    <input type="email" name="email" placeholder="@lang('Enter your email')" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                    <span class="text-danger"><small>{{$errors->first('email')}}</small></span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <textarea class="area-style-1 color-1" name="comment" placeholder="@lang('Enter your comment')">{{old('comment')}}</textarea>
                                @if($errors->has('comment'))
                                    <span class="text-danger"><small>{{$errors->first('comment')}}</small></span>
                                    @endif

                            </div>
                            <div class="col-xs-12">
                                    @if(env('GOOGLE_RECAPTCHA_KEY'))
                                            <div class="g-recaptcha"
                                                data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                                            </div>

                                            @if($errors->has('g-recaptcha-response'))
                                    <span class="text-danger"><small>{{$errors->first('g-recaptcha-response')}}</small></span>
                                    @endif
                                    @endif
                            </div>
                            <div class="col-xs-12">
                                    <button type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o"><span>@lang('submit comment')</span></button>
                            </div>
                        </div>
                    </form>
                    <div class="ajax-result">
                        <div class="success"></div>
                        <div class="error"></div>
                    </div>
                    <div class="ajax-loader"></div>
                </div>
                <div class="col-xs-12 col-sm-5">

                    <div class="contact-info">
                        <h4 class="color-dark-2"><strong>@lang('contact info')</strong></h4>
                    <div class="contact-line color-grey-3"><img src="img/phone_icon_2_dark.png" alt="">@lang('Phone'): <a class="color-dark-2" href="tel:{{$contact->phone}}">{{$contact->phone}}</a></div>
                    <div class="contact-line color-grey-3"><img src="img/mail_icon_b_dark.png" alt="">@lang('Email us'): <a class="color-dark-2 tt" href="mailto:{{$contact->email}}">{{$contact->email}}</a></div>
                    <div class="contact-line color-grey-3"><img src="img/loc_icon_dark.png" alt="">@lang('Address'): <span class="color-dark-2 tt">{{translate($contact, 'address')}}</span></div>
                    </div>
                    <div class="contact-socail">
                        <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-skype"></i></a>
                        <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-google-plus"></i></a>

                        <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-instagram"></i></a>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="map-block">
<iframe src="{{$contact->map_url}}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
@endsection

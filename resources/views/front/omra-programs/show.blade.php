@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>translate($program, 'title'), 'description'=>translate($program,
'description') ])
@endsection

@section('content')
@include('front.includes.inner-banner', [ 'title'=>'Umrah Programs',
'image'=>getPageHeaderImage('umrah'), 'crumbs'=>[ ['title'=>'Umrah Programs', 'url'=>i18nUrl('umra-programs')], ['title'=>translate($program,
'title'), 'url'=>''] ] ])

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="blog-list">
                    <div>
                        <div class="blog-list-top">
                            <img class="img-responsive img-full" src="{{$program->image['original']}}" alt="">
                        </div>
                        <p><b>Price: </b>{{$program->price}}</p>


                        <div class="blog-list-text">
                            {!! translate($program, 'body') !!}
                        </div>

                        <a class="btn btn-success  b-40 bg-white color-dark-2 hv-dark-2-o grid-hidden" href="{{i18nUrl('programs/'.$program->slug.'/book')}}">@lang('Book now')</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Book now') ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'', 'image'=>getPageHeaderImage('booking') ])



<div class="main-wraper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-dr-blue-2 underline">@lang('Booking')</h4>
                    <h2>@lang('Kalovida Travel')</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <form class="simple-from" method="POST" action="{{i18nUrl('programs/'.$program->slug.'/book')}}">
                    @csrf
                    <div class="simple-group">
                        <h3 class="small-title">@lang('Selected Program')</h3>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-block type-2 clearfix">
                                    <div class="list-item-entry">
                                        <div class="hotel-item style-8 bg-white">
                                            <div class="table-view">
                                                <div class="radius-top cell-view">
                                                    <img src="{{$program->image['thumb']}}" alt="{{ translate($program, 'title') }}">

                                                </div>
                                                <div class="title hotel-middle clearfix cell-view">


                                                    <h4><a href="{{i18nUrl('hajj-programs/'.$program->slug)}}"><b>{{ translate($program, 'title') }}</b></a></h4>
                                                    <p class="f-14">{{ translate($program, 'description') }}</p>


                                                </div>
                                                <div class="title hotel-right bg-dr-blue clearfix cell-view">
                                                    <div class="hotel-person color-white"> <span>${{$program->price}}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- <div class="simple-group">
                        <h3 class="small-title">Your Card Information</h3>
                        <div class="row">

                            <div class="col-xs-12 col-sm-6">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2">Card Holder Name</div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="text" placeholder="Enter your card holder name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2">Card Number</div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="text" placeholder="Enter your card number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2">Card Identification Number</div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="text" placeholder="Enter your e-mail adress for verify">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2">Credit Card Type</div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="drop-wrap drop-wrap-s-4 color-5">
                                                <div class="drop">
                                                    <b>Month</b>
                                                    <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                                    <span>
													    <a href="#">January</a>
														<a href="#">February</a>
													    <a href="#">March</a>
														<a href="#">April</a>
														<a href="#">May</a>
														<a href="#">June</a>
														<a href="#">Jul</a>
														<a href="#">July</a>
														<a href="#">August</a>
														<a href="#">September</a>
														<a href="#">October</a>
														<a href="#">November</a>
														<a href="#">December</a>
													</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="drop-wrap drop-wrap-s-4 color-5">
                                                <div class="drop">
                                                    <b>Year</b>
                                                    <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                                    <span>
													    <a href="#">2015</a>
														<a href="#">2016</a>
													    <a href="#">2017</a>
														<a href="#">2018</a>
														<a href="#">2019</a>
														<a href="#">2020</a>
													</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="confirm-terms">
                            <div class="input-entry color-3">
                                <input class="checkbox-form" id="text-2" type="checkbox" name="checkbox" value="climat control">
                                <label class="clearfix" for="text-2">
										<span class="sp-check"><i class="fa fa-check"></i></span>
										<span class="checkbox-text">By continuing, you agree to the<a class="color-dr-blue-2 link-dark-2" href="#"> Terms and Conditions</a>.</span>
									</label>
                            </div>
                        </div>
                    </div> -->
                    <input type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="confirm booking">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

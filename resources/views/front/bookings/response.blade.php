@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Book now') ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'', 'image'=>getPageHeaderImage('booking') ])



<div class="main-wraper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-dr-blue-2 underline">@lang('Booking')</h4>
                    <h2>@lang('Kalovida Travel')</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                
                    <div class="simple-group">
                        <h3 class="small-title">@lang('Program Booking')</h3>
                        <div class="row">
                            <div class="col-xs-12">
                                <p>@lang('Your booking request has been sent, we will contact you in 24 hours.')</p>
                                <p>@lang('Program:') {{translate($program, 'title')}}</p>
                                <p>@lang('Thank you.')</p>
                            </div>

                        </div>
                    </div>

                    
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

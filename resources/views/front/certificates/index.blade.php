@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>trans('Certificates') ])
@endsection

@section('content')
    @include('front.includes.inner-banner',
[ 'title'=>'Certificates', 'image'=>getPageHeaderImage('certificates'), 'crumbs'=>[ ['title'=>'Certificates', 'url'=>''] ] ])

<div class="main-wraper padd-70-0">

        <div class="filter-content row" style="position: relative; height: 3095.86px;">
            <div class="grid-sizer col-mob-12 col-xs-6 col-sm-4 no-padding"></div>
            @foreach($certificates as $certificate)
            <div class="item tours gal-item style-2 col-mob-12 col-xs-6 col-sm-4 no-padding" style="position: absolute; left: 0px; top: 0px;">
                <a class="black-hover" href="{{$certificate->image['original']}}" data-lightbox="certifcates">
                    <img class="img-full img-responsive" src="{{$certificate->image['thumb']}}" alt="">
                    <div class="tour-layer delay-1"></div>
                    <div class="vertical-align">
                        <h3 class="color-white"><b>{{translate($certificate, 'title')}}</b></h3>
                        <h5 class="color-white">{{translate($certificate, 'description')}}</h5>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
@endsection

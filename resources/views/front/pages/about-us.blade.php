@extends('front.layouts.master')
@section('meta')
@meta([
        'title'=>translate($page, 'title'),
        'description'=>translate($page, 'description')
    ])
@endsection

@section('content')

@include('front.includes.inner-banner', [ 'title'=>translate($page,'title'), 'image'=>getPageHeaderImage('about_us'), 'crumbs'=>[ ['title'=>translate($page,'title'), 'url'=>''] ] ])

<div class="main-wraper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                <h4 class="subtitle color-dr-blue-2 underline">{{translate($page, 'title')}}</h4>
                    <h2>@lang('Kalovida Travel')</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7">
                <div class="simple-text">
                    {!!translate($page, 'body')!!}
                </div>
            </div>

            <div class="col-mob-12 col-xs-6 col-sm-5 col-sm-offset-1 col-md-5 col-md-offset-0">
            <img class="img-responsive img-full" src="{{$page->image['original']}}" alt="">
            </div>
        </div>
    </div>
</div>



<!-- ICON-BLOCK -->



<!-- PARTNERS -->
<div class="main-wraper padd-90">
    @include('front.includes.partners-widget')
</div>
@endsection

@extends('front.layouts.master')
@section('meta')
@meta([
        'title'=>translate($page, 'title'),
        'description'=>translate($page, 'description')
    ])
@endsection

@section('content')
@include('front.includes.inner-banner', [ 'title'=>translate($page,'title'), 'image'=>getPageHeaderImage('gallery'), 'crumbs'=>[ ['title'=>translate($page,
'title'), 'url'=>''] ] ])



<div class="detail-wrapper">
	<div class="container">

       	<div class="row padd-90">
       		<div class="col-xs-12 col-md-12">
       			<div class="detail-content color-1">


					<div class="detail-content-block">
						{!!translate($page, 'body')!!}
					</div>

				</div>
       		</div>

       	</div>

	</div>
</div>



@endsection



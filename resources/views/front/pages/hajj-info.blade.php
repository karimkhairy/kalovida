@extends('front.layouts.master')
@section('meta')
@meta([
        'title'=>translate($page, 'title'),
        'description'=>translate($page, 'description')
    ])
@endsection

@section('content')
@include('front.includes.inner-banner', [ 'title'=>translate($page,'title'), 'image'=>getPageHeaderImage('hajj'), 'crumbs'=>[ ['title'=>translate($page,
'title'), 'url'=>''] ] ])



<div class="row padd-90">
        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
            <div class="blog-list">
             <div>
                 <div class="blog-list-top">
                 <img class="img-responsive img-full" src="{{$page->image['original']}}" alt="{{translate($page,'title')}}">
                 </div>


                 <div class="blog-list-text" style="text-align: right ">{!!translate($page, 'body')!!}</div>

             </div>

         </div>
        </div>
    </div>


@endsection

@extends('front.layouts.master')
@section('meta') @meta([ 'title'=>translate($service, 'title'), 'description'=>translate($service,
'description') ])
@endsection

@section('content')
    @include('front.includes.inner-banner', [ 'title'=>translate($service,
'title'), 'image'=>getPageHeaderImage('dubai_services'), 'crumbs'=>[ ['title'=>'Services', 'url'=>i18nUrl('services')], ['title'=>translate($service,
'title'), 'url'=>''] ] ])

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="blog-list">
                    <div>
                        <div class="blog-list-top">
                            <img class="img-responsive img-full" src="{{$service->image['original']}}" alt="">
                        </div>


                        <div class="blog-list-text">
                            {!! translate($service, 'body') !!}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@component('mail::message')
# Job request from ({{$mes->name}})

@component('mail::panel')
<b>Phone:</b> {{$mes->phone}}
@endcomponent
@component('mail::panel')
<b>Address:</b> {{$mes->address}}
@endcomponent
@component('mail::panel')
<b>Details:</b><br/>
{{$mes->details}}
@endcomponent




Thanks,<br>
{{ config('app.name') }}
@endcomponent

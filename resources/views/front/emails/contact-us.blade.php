@component('mail::message')
# Message from ({{$mes->name}})

{{$mes->comment}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent

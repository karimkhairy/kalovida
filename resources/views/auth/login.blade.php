@extends('front.layouts.app')

@section('content')
<div class="login-fullpage">
    <div class="row">
        <div class="login-logo"><img class="center-image" src="{{ asset('front/img/login.jpg')}}" alt=""></div>
        <div class="col-xs-12 col-sm-7">
            <div class="f-login-content">
                <div class="f-login-header">
                    <div class="f-login-title color-dr-blue-2">@lang('Welcome')!</div>
                    <div class="f-login-desc color-grey">@lang('Please login to your account')</div>
                </div>
                <form method="POST" action="{{ route('login') }}" class="f-login-form">
                    @csrf
                    <div class="input-style-1 b-50 type-2 color-5">
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="@lang('E-Mail')">

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="input-style-1 b-50 type-2 color-5">
                        <input id="password" type="password" name="password" required placeholder="@lang('password')">

                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    @endif
                    <a class="btn btn-link" href="{{ route('register') }}">
                        {{ __('Register') }}
                    </a>


                    <input type="submit" class="login-btn c-button full b-60 bg-dr-blue-2 hv-dr-blue-2-o" value="LOGIN TO YOUR ACCOUNT">


                </form>
            </div>
        </div>
    </div>
</div>

@endsection

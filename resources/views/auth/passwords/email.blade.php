@extends('front.layouts.app')

@section('content')

<div class="login-fullpage">
    <div class="row">
        <div class="login-logo"><img class="center-image" src="{{ asset('front/img/login.jpg')}}" alt=""></div>
        <div class="col-xs-12 col-sm-7">
            <div class="f-login-content">
                <div class="f-login-header">
                    <div class="f-login-title color-dr-blue-2">@lang('Reset Password')!</div>
                    <div class="f-login-desc color-grey">@lang('Please fill all the fields')</div>
                </div>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('password.email') }}" class="f-login-form">
                    @csrf


                    <div class="input-style-1 b-50 type-2 color-5">
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="@lang('E-Mail')">

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>



                    <input type="submit" class="login-btn c-button full b-60 bg-dr-blue-2 hv-dr-blue-2-o" value="@lang('Send Password Reset Link')">


                </form>
            </div>
        </div>
    </div>
</div>

@endsection

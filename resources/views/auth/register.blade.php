@extends('front.layouts.app')

@section('content')

<div class="login-fullpage">
    <div class="row">
        <div class="login-logo"><img class="center-image" src="{{ asset('front/img/login.jpg')}}" alt=""></div>
        <div class="col-xs-12 col-sm-7">
            <div class="f-login-content">
                <div class="f-login-header">
                    <div class="f-login-title color-dr-blue-2">@lang('Register new account')!</div>
                    <div class="f-login-desc color-grey">@lang('Please fill all the fields')</div>
                </div>
                <form method="POST" action="{{ route('register') }}" class="f-login-form">
                    @csrf
                    <div class="input-style-1 b-50 type-2 color-5">
                    <input id="name" name="name" value="{{ old('name') }}" required autofocus placeholder="@lang('Name')" >

                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="input-style-1 b-50 type-2 color-5">
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="@lang('E-Mail')">

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="input-style-1 b-50 type-2 color-5">
                        <input id="password" type="password" name="password" required placeholder="@lang('Password')">

                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="input-style-1 b-50 type-2 color-5">
                        <input id="password_confirmation" type="password" name="password_confirmation" required placeholder="@lang('Password Confirmation')">

                        @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>

                    <input type="submit" class="login-btn c-button full b-60 bg-dr-blue-2 hv-dr-blue-2-o" value="@lang('Register')">


                </form>
            </div>
        </div>
    </div>
</div>

@endsection
